<?php
/**
 * Description :
 * This class allows to define standard validation class.
 * Standard validation uses validator object,
 * to validate data.
 *
 * Standard validation uses the following specified configuration:
 * [
 *     Default validation configuration,
 *
 *     data(optional: got [], if not found): [
 *         @see StandardValidation::getTabData() return array format
 *     ],
 *
 *     rule_config(optional: got [], if not found): [
 *         @see StandardValidation::getTabRuleConfig() return array format
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\standard\model;

use liberty_code\validation\validation\model\DefaultValidation;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validation\library\ConstValidation;
use liberty_code\validation\validation\standard\library\ConstStandardValidation;
use liberty_code\validation\validation\standard\exception\ValidatorInvalidFormatException;
use liberty_code\validation\validation\standard\exception\ConfigInvalidFormatException;



/**
 * @method ValidatorInterface getObjValidator() Get validator object.
 * @method void setObjValidator(ValidatorInterface $objValidator) Set validator object.
 */
class StandardValidation extends DefaultValidation
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ValidatorInterface $objValidator
     */
    public function __construct(
        ValidatorInterface $objValidator,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct($tabConfig);

        // Init validator
        $this->setObjValidator($objValidator);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStandardValidation::DATA_KEY_DEFAULT_VALIDATOR))
        {
            $this->__beanTabData[ConstStandardValidation::DATA_KEY_DEFAULT_VALIDATOR] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStandardValidation::DATA_KEY_DEFAULT_VALIDATOR
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStandardValidation::DATA_KEY_DEFAULT_VALIDATOR:
                    ValidatorInvalidFormatException::setCheck($value);
                    break;

                case ConstValidation::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabData()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstStandardValidation::TAB_CONFIG_KEY_DATA]) ?
                $tabConfig[ConstStandardValidation::TAB_CONFIG_KEY_DATA] :
                array()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabRuleConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstStandardValidation::TAB_CONFIG_KEY_RULE_CONFIG]) ?
                $tabConfig[ConstStandardValidation::TAB_CONFIG_KEY_RULE_CONFIG] :
                array()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabErrorEngine(
        array &$tabErrorMessage,
        array &$tabErrorException
    )
    {
        // Init var
        $objValidator = $this->getObjValidator();
        $tabData = $this->getTabData();
        $tabRuleConfig = $this->getTabRuleConfig();

        // Get errors
        $objValidator->checkTabDataIsValid(
            $tabData,
            $tabRuleConfig,
            $tabErrorMessage,
            $tabErrorException
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set specified data array.
     *
     * Data array format:
     * @see getTabData() return array format.
     *
     * @param array $tabData
     */
    public function setData(array $tabData)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set data
        $tabConfig[ConstStandardValidation::TAB_CONFIG_KEY_DATA] = $tabData;

        // Set configuration
        $this->setConfig($tabConfig);
    }



    /**
     * Set specified rule configurations array.
     *
     * Rule configurations array format:
     * @see getTabRuleConfig() return array format.
     *
     * @param array $tabConfig
     */
    public function setRuleConfig(array $tabConfig)
    {
        // Init var
        $tabRuleConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();

        // Set rule configurations
        $tabConfig[ConstStandardValidation::TAB_CONFIG_KEY_RULE_CONFIG] = $tabRuleConfig;

        // Set configuration
        $this->setConfig($tabConfig);
    }



}