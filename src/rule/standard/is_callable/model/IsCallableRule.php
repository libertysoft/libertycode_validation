<?php
/**
 * Description :
 * This class allows to define is callable rule class.
 * Is callable rule allows to check if specified data is callable.
 *
 * Is callable rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Is callable rule uses the following specified error configuration:
 * [
 *     Default rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\is_callable\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\is_callable\library\ConstIsCallableRule;



class IsCallableRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Return result
        return is_callable($value);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstIsCallableRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstIsCallableRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}