<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\sub_rule\library;



class ConstSubRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_RULE_CONFIG = 'rule_config';

    // Error configuration
    const TAB_ERROR_CONFIG_KEY_RULE_CONFIG = 'rule_config';


	
    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the sub-rule validation configuration standard.';
    const EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the sub-rule error configuration standard.';



}