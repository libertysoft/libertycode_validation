<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\library;



class ConstRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
	
	

    // Configuration
    const TAB_CONFIG_KEY_KEY = 'key';
    const TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN = 'error_message_pattern';

    // Error configuration
    const TAB_ERROR_CONFIG_KEY_ERROR_MESSAGE_PATTERN = 'error_message_pattern';

    const ERROR_MESSAGE_PATTERN_ARG_DEFAULT = '';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default rule configuration standard.';
    const EXCEPT_MSG_DATA_NAME_INVALID_FORMAT = 'Following data name "%1$s" invalid! The name must be a string not empty.';
    const EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default rule error configuration standard.';
    const EXCEPT_MSG_VALIDATION_FAIL = 'Validation failed!%1$s';
    const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
    const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be a rule object in collection.';



}