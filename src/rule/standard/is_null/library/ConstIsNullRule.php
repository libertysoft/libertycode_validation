<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\is_null\library;



class ConstIsNullRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'is_null';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s is not null.';



}