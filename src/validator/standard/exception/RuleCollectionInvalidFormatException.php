<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\standard\exception;

use Exception;

use liberty_code\validation\rule\api\RuleCollectionInterface;
use liberty_code\validation\validator\standard\library\ConstStandardValidator;



class RuleCollectionInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $collection
     */
	public function __construct($collection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStandardValidator::EXCEPT_MSG_RULE_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($collection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified collection has valid format.
	 * 
     * @param mixed $collection
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($collection)
    {
		// Init var
		$result = (
            (!is_null($collection)) &&
			($collection instanceof RuleCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($collection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}