<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\exception;

use Exception;

use liberty_code\validation\validator\library\ConstValidator;



class DataNameInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $name
     */
	public function __construct($name)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstValidator::EXCEPT_MSG_DATA_NAME_INVALID_FORMAT,
            mb_strimwidth(strval($name), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified name has valid format.
     *
     * @param mixed $name
     * @return boolean
     */
    public static function checkNameIsValid($name)
    {
        // Return result
        return (is_string($name) && (trim($name) != ''));
    }



	/**
	 * Check if specified name has valid format.
	 * 
     * @param mixed $name
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($name)
    {
		// Init var
		$result = static::checkNameIsValid($name);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($name);
		}
		
		// Return result
		return $result;
    }
	
	
	
}