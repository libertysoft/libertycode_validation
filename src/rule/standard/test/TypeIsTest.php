<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/rule/standard/test/RuleCollectionTest.php');

// Use
use liberty_code\validation\rule\api\RuleCollectionInterface;



// Test data validation
echo('Test validation : <br />');

$tabInfo = array(
    // Test string Ko
    [
        'type_string',
        'key_1',
        7,
        []
    ],

    // Test string Ok
    [
        'type_string',
        'key_1',
        'Value 1',
        null
    ],

    // Test string Ok
    [
        'type_string',
        'key_1',
        '',
        null
    ],

    // Test numeric Ko
    [
        'type_numeric',
        'key_2',
        '7.1',
        ['integer_only_require' => 1]
    ],

    // Test numeric Ok
    [
        'type_numeric',
        'key_2',
        '7',
        ['integer_only_require' => 1]
    ],

    // Test numeric Ok
    [
        'type_numeric',
        'key_2',
        7.1,
        null
    ],

    // Test boolean Ko
    [
        'type_boolean',
        'key_1',
        'Value 1',
        null
    ],

    // Test boolean Ok
    [
        'type_boolean',
        'key_1',
        '1',
        null
    ],

    // Test boolean Ko
    [
        'type_boolean',
        'key_1',
        '1',
        [
            'integer_enable_require' => 1,
            'string_enable_require' => false
        ]
    ],

    // Test boolean Ok
    [
        'type_boolean',
        'key_1',
        0,
        [
            'integer_enable_require' => 1,
            'string_enable_require' => false
        ]
    ],

    // Test array Ko
    [
        'type_array',
        'key_2',
        [
            'key_1' => 'Value 1',
            'key_2' => 'Value 2',
            'key_3' => 'Value 3'
        ],
        ['index_only_require' => '1']
    ],

    // Test array Ok
    [
        'type_array',
        'key_2',
        [
            'key_1' => 'Value 1',
            'key_2' => 'Value 2',
            'key_3' => 'Value 3'
        ],
        ['index_only_require' => '0']
    ],

    // Test array Ok
    [
        'type_array',
        'key_2',
        [
            'key_1' => 'Value 1',
            'key_2' => 'Value 2',
            'key_3' => 'Value 3'
        ],
        null
    ],

    // Test array Ok
    [
        'type_array',
        'key_2',
        [
            'Value 1',
            'Value 2',
            2 => 'Value 3'
        ],
        ['index_only_require' => true]
    ],

    // Test date Ko
    [
        'type_date',
        'key_1',
        'Value 1',
        null
    ],

    // Test date Ok
    [
        'type_date',
        'key_1',
        new DateTime(),
        null
    ],

    // Test object Ko
    [
        'type_object',
        'key_2',
        7,
        null
    ],

    // Test object Ok
    [
        'type_object',
        'key_2',
        $objRuleCollection,
        null
    ],

    // Test object Ko
    [
        'type_object',
        'key_1',
        $objRuleCollection,
        [
            'class_path' => [RuleCollectionInterface::class],
            'extend_enable_require' => '0'
        ]
    ],

    // Test object Ok
    [
        'type_object',
        'key_1',
        $objRuleCollection,
        ['class_path' => [RuleCollectionInterface::class]]
    ],

    // Test is null Ko
    [
        'is_null',
        'key_1',
        'Value 1',
        null
    ],

    // Test is null Ok
    [
        'is_null',
        'key_1',
        null,
        null
    ],

    // Test is callable Ko
    [
        'is_callable',
        'key_2',
        'Value 2',
        null
    ],

    // Test is callable Ok
    [
        'is_callable',
        'key_2',
        function(){return 'Value 2';},
        null
    ],

    // Test is empty Ko
    [
        'is_empty',
        'key_1',
        'Value 1',
        null
    ],

    // Test is empty Ok
    [
        'is_empty',
        'key_1',
        '    ',
        null
    ],

    // Test is empty Ko
    [
        'is_empty',
        'key_2',
        7,
        null
    ],

    // Test is empty Ok
    [
        'is_empty',
        'key_2',
        0.000,
        null
    ],

    // Test is empty Ko
    [
        'is_empty',
        'key_3',
        array('Value 3'),
        null
    ],

    // Test is empty Ok
    [
        'is_empty',
        'key_3',
        array(),
        null
    ],

    // Test is empty Ko
    [
        'is_empty',
        'key_4',
        $objRuleCollection,
        null
    ],

    // Test is empty Ok
    [
        'is_empty',
        'key_4',
        null,
        null
    ]
);

foreach($tabInfo as $info)
{
    echo('Test validation info: <br />');

    try{
        // Get info
        $strKey = $info[0];
        $strName = $info[1];
        $value = $info[2];
        $tabConfig = $info[3];

        // Get rule
        $boolExists = $objRuleCollection->checkExists($strKey);
        $objRule = $objRuleCollection->getObjRule($strKey);

        echo('Info: <pre>');
        print_r(array(
            $strKey,
            $strName,
            (is_object($value) ? get_class($value) : $value),
            $tabConfig
        ));
        echo('</pre>');

        // Test validation, if required
        if($boolExists)
        {
            // Get validation
            $boolIsValid = $objRule->checkIsValid($strName, $value, $tabConfig);
            $strErrorMessage = $objRule->getStrErrorMessage($strName, $value, $tabConfig);
            $objErrorException = $objRule->getObjErrorException($strName, $value, $tabConfig);

            echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
            echo('Get error message: <pre>');var_dump($strErrorMessage);echo('</pre>');
            echo('Get error exception: <pre>');
            var_dump(
                (!is_null($objErrorException)) ?
                    get_class($objErrorException) . ': ' . $objErrorException->getMessage() :
                    $objErrorException
            );
            echo('</pre>');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


