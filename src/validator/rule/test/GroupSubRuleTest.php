<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/rule/standard/test/RuleCollectionTest.php');



// Test data validation
echo('Test validation : <br />');

$tabInfo = array(
    // Test and Ko
    [
        'group_sub_rule_and',
        'key_1',
        'Value 1',
        null
    ],

    // Test and Ko
    [
        'group_sub_rule_and',
        'key_1',
        -1,
        [
            'rule_config' => [
                'is-valid-numeric' => [
                    'type_numeric',
                    [
                        'compare_less',
                        [
                            'compare_value' => 7,
                            'equal_enable_require' => false
                        ]
                    ]
                ],
                'is-positive-integer' => [
                    [
                        'type_numeric',
                        'integer_only_require' => 1
                    ],
                    [
                        'compare_greater',
                        [
                            'compare_value' => 0,
                            'equal_enable_require' => true
                        ]
                    ]
                ]
            ]
        ]
    ],

    // Test and Ko
    [
        'group_sub_rule_and',
        'key_1',
        7,
        [
            'rule_config' => [
                'is-valid-numeric' => [
                    'type_numeric',
                    [
                        'compare_less',
                        [
                            'compare_value' => 7,
                            'equal_enable_require' => false
                        ]
                    ]
                ],
                'is-positive-integer' => [
                    [
                        'type_numeric',
                        'integer_only_require' => 1
                    ],
                    [
                        'compare_greater',
                        [
                            'compare_value' => 0,
                            'equal_enable_require' => true
                        ]
                    ]
                ]
            ]
        ]
    ],

    // Test and Ok
    [
        'group_sub_rule_and',
        'key_1',
        5,
        [
            'rule_config' => [
                'is-valid-numeric' => [
                    'type_numeric',
                    [
                        'compare_less',
                        [
                            'compare_value' => 7,
                            'equal_enable_require' => false
                        ]
                    ]
                ],
                'is-positive-integer' => [
                    [
                        'type_numeric',
                        'integer_only_require' => 1
                    ],
                    [
                        'compare_greater',
                        [
                            'compare_value' => 0,
                            'equal_enable_require' => true
                        ]
                    ]
                ]
            ]
        ]
    ],

    // Test or Ko
    [
        'group_sub_rule_or',
        'key_1',
        7,
        [
            'rule_config' => [
                'is-null' => [
                    'is_null'
                ],
                'is-valid-string' => [
                    'type_string',
                    [
                        'sub_rule_not',
                        ['rule_config' => ['is_empty']]
                    ]
                ]
            ]
        ]
    ],


    // Test or Ok
    [
        'group_sub_rule_or',
        'key_1',
        null,
        [
            'rule_config' => [
                'is-null' => [
                    'is_null'
                ],
                'is-valid-string' => [
                    'type_string',
                    [
                        'sub_rule_not',
                        ['rule_config' => ['is_empty']]
                    ]
                ]
            ]
        ]
    ],

    // Test or Ko
    [
        'group_sub_rule_or',
        'key_1',
        '',
        [
            'rule_config' => [
                'is-null' => [
                    'is_null'
                ],
                'is-valid-string' => [
                    'type_string',
                    [
                        'sub_rule_not',
                        ['rule_config' => ['is_empty']]
                    ]
                ]
            ]
        ]
    ],

    // Test or Ok
    [
        'group_sub_rule_or',
        'key_1',
        'Value 1',
        [
            'rule_config' => [
                'is-null' => [
                    'is_null'
                ],
                'is-valid-string' => [
                    'type_string',
                    [
                        'sub_rule_not',
                        ['rule_config' => ['is_empty']]
                    ]
                ]
            ]
        ]
    ]
);

foreach($tabInfo as $info)
{
    echo('Test validation info: <br />');

    try{
        // Get info
        $strKey = $info[0];
        $strName = $info[1];
        $value = $info[2];
        $tabConfig = $info[3];

        // Get rule
        $boolExists = $objRuleCollection->checkExists($strKey);
        $objRule = $objRuleCollection->getObjRule($strKey);

        echo('Info: <pre>');
        print_r(array(
            $strKey,
            $strName,
            (is_object($value) ? get_class($value) : $value),
            $tabConfig
        ));
        echo('</pre>');

        // Test validation, if required
        if($boolExists)
        {
            // Get validation
            $boolIsValid = $objRule->checkIsValid($strName, $value, $tabConfig);
            $strErrorMessage = $objRule->getStrErrorMessage($strName, $value, $tabConfig);
            $objErrorException = $objRule->getObjErrorException($strName, $value, $tabConfig);

            echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
            echo('Get error message: <pre>');var_dump($strErrorMessage);echo('</pre>');
            echo('Get error exception: <pre>');
            var_dump(
                (!is_null($objErrorException)) ?
                    get_class($objErrorException) . ': ' . $objErrorException->getMessage() :
                    $objErrorException
            );
            echo('</pre>');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


