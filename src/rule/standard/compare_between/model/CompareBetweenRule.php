<?php
/**
 * Description :
 * This class allows to define compare between rule class.
 * Compare between rule allows to check if specified data is between than specified compare values.
 *
 * Compare between rule uses the following specified configuration:
 * [
 *     Default rule configuration,
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name,
 *         '%2$s' replaced by data value,
 *         '%3$s' replaced by greater compare value,
 *         '%4$s' replaced by less compare value"
 * ]
 *
 * Compare between rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     greater_compare_value(required): mixed greater value to compare,
 *
 *     greater_equal_enable_require(optional: got true if not found): true / false,
 *
 *     less_compare_value(required): mixed less value to compare,
 *
 *     less_equal_enable_require(optional: got true if not found): true / false
 * ]
 *
 * Compare between rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     greater_compare_value(required): mixed greater value to compare,
 *
 *     less_equal_enable_require(required): mixed less value to compare,
 *
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name,
 *         '%2$s' replaced by data value,
 *         '%3$s' replaced by greater compare value,
 *         '%4$s' replaced by less compare value"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\compare_between\model;

use liberty_code\validation\rule\model\DefaultRule;

use DateTime;
use liberty_code\library\str\library\ToolBoxString;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\compare_between\library\ConstCompareBetweenRule;
use liberty_code\validation\rule\standard\compare_between\exception\ValidConfigInvalidFormatException;
use liberty_code\validation\rule\standard\compare_between\exception\ErrorConfigInvalidFormatException;



class CompareBetweenRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check greater equal enable required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see CompareBetweenRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkGreaterEqualEnableRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            (!is_array($tabValidConfig)) ||
            (!array_key_exists(ConstCompareBetweenRule::TAB_VALID_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE, $tabValidConfig)) ||
            (intval($tabValidConfig[ConstCompareBetweenRule::TAB_VALID_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check less equal enable required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see CompareBetweenRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkLessEqualEnableRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            (!is_array($tabValidConfig)) ||
            (!array_key_exists(ConstCompareBetweenRule::TAB_VALID_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE, $tabValidConfig)) ||
            (intval($tabValidConfig[ConstCompareBetweenRule::TAB_VALID_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $greaterCompareValue = $tabConfig[ConstCompareBetweenRule::TAB_VALID_CONFIG_KEY_GREATER_COMPARE_VALUE];
            $boolGreaterEqualEnable = $this->checkGreaterEqualEnableRequired($tabConfig);
            $lessCompareValue = $tabConfig[ConstCompareBetweenRule::TAB_VALID_CONFIG_KEY_LESS_COMPARE_VALUE];
            $boolLessEqualEnable = $this->checkLessEqualEnableRequired($tabConfig);
            $result = (
                // Check valid value
                (
                    is_string($value) ||
                    is_numeric($value) ||
                    ($value instanceof DateTime)
                ) &&

                // Check value greater (equal) than compare value
                (
                $boolGreaterEqualEnable ?
                    ($value >= $greaterCompareValue) :
                    ($value > $greaterCompareValue)
                ) &&

                // Check value less (equal) than compare value
                (
                    $boolLessEqualEnable ?
                        ($value <= $lessCompareValue) :
                        ($value < $lessCompareValue)
                )
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstCompareBetweenRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstCompareBetweenRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = null;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strValue = (
                ($value instanceof DateTime) ?
                    $value->format('Y-m-d H:i:s') :
                    (
                        ToolBoxString::checkConvertString($value) ?
                            strval($value) :
                            ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
                    )
            );
            $greaterCompareValue = $tabConfig[ConstCompareBetweenRule::TAB_VALID_CONFIG_KEY_GREATER_COMPARE_VALUE];
            $strGreaterCompareValue = (
                ($greaterCompareValue instanceof DateTime) ?
                    $greaterCompareValue->format('Y-m-d H:i:s') :
                    strval($greaterCompareValue)
            );
            $lessCompareValue = $tabConfig[ConstCompareBetweenRule::TAB_VALID_CONFIG_KEY_LESS_COMPARE_VALUE];
            $strLessCompareValue = (
                ($lessCompareValue instanceof DateTime) ?
                    $lessCompareValue->format('Y-m-d H:i:s') :
                    strval($lessCompareValue)
            );
            $result = sprintf($strErrorMsgPattern, $strName, $strValue, $strGreaterCompareValue, $strLessCompareValue);
        }

        // Return result
        return $result;
    }



}