<?php
/**
 * Description :
 * This class allows to define default validation class.
 * Can be consider is base of all validation types.
 *
 * Default validation uses the following specified configuration:
 * []
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\validation\validation\api\ValidationInterface;

use liberty_code\validation\validation\library\ConstValidation;
use liberty_code\validation\validation\exception\ConfigInvalidFormatException;
use liberty_code\validation\validation\exception\DataNameInvalidFormatException;



abstract class DefaultValidation extends FixBean implements ValidationInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /** @var null|array */
    protected $tabErrorMessage;



    /** @var null|array */
    protected $tabErrorException;
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }

        // Hydrate errors
        $this->hydrateError();
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init var
        if(!$this->beanExists(ConstValidation::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstValidation::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }



    /**
     * Hydrate errors array.
     */
    public function hydrateError()
    {
        // Init var
        $this->tabErrorMessage = null;
        $this->tabErrorException = null;
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstValidation::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstValidation::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Set check if specified data name is valid.
     *
     * @param null|string $strName = null
     * @throws DataNameInvalidFormatException
     */
    protected function setCheckDataNameIsValid($strName = null)
    {
        // Set check argument
        DataNameInvalidFormatException::setCheck($strName);

        // Init var
        $tabData = $this->getTabData();
        $isValid = (
            is_null($strName) ||
            array_key_exists($strName, $tabData)
        );

        if(!$isValid) {
            throw new DataNameInvalidFormatException($strName);
        }
    }



    /**
     * @inheritdoc
     * @throws DataNameInvalidFormatException
     */
    public function checkDataIsValid($strName = null)
    {
        // Init var
        $tabErrorMessage = $this->getTabErrorMessage($strName);
        $tabErrorException = $this->getTabErrorException($strName);
        $result = (
            (count($tabErrorMessage) === 0) &&
            (count($tabErrorException) === 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstValidation::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get errors array engine.
     *
     * @param array &$tabErrorMessage
     * @param array &$tabErrorException
     */
    abstract protected function getTabErrorEngine(
        array &$tabErrorMessage,
        array &$tabErrorException
    );



    /**
     * @inheritdoc
     * @throws DataNameInvalidFormatException
     */
    public function getTabErrorMessage($strName = null)
    {
        // Set check argument
        $this->setCheckDataNameIsValid($strName);

        // Set errors
        $this->setError();

        // Init var
        $result = (
            is_string($strName) ?
                (
                    isset($this->tabErrorMessage[$strName]) ?
                        $this->tabErrorMessage[$strName] :
                        array()
                ) :
                $this->tabErrorMessage
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws DataNameInvalidFormatException
     */
    public function getTabErrorException($strName = null)
    {
        // Set check argument
        $this->setCheckDataNameIsValid($strName);

        // Set errors
        $this->setError();

        // Init var
        $result = (
            is_string($strName) ?
                (
                    isset($this->tabErrorException[$strName]) ?
                        $this->tabErrorException[$strName] :
                        array()
                ) :
                $this->tabErrorException
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        // Hydrate errors
        $this->hydrateError();

        // Set configuration
        $this->beanSet(ConstValidation::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



    /**
     * Set errors arrays.
     */
    protected function setError()
    {
        // Set errors, if required
        if(
            is_null($this->tabErrorMessage) ||
            is_null($this->tabErrorException)
        )
        {
            // Get errors
            $tabErrorMessage = array();
            $tabErrorException = array();
            $this->getTabErrorEngine(
                $tabErrorMessage,
                $tabErrorException
            );

            // Set errors
            $this->tabErrorMessage = $tabErrorMessage;
            $this->tabErrorException = $tabErrorException;
        }
    }



}