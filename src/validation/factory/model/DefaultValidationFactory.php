<?php
/**
 * Description :
 * This class allows to define default validation factory class.
 * Can be consider is base of all validation factory type.
 *
 * Default validation factory uses the following specified configuration, to get and hydrate validation:
 * [
 *     type(optional): "string constant to determine validation type",
 *
 *     config(optional): [
 *         ... specific @see ValidationInterface configuration array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\validation\validation\factory\api\ValidationFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validation\api\ValidationInterface;
use liberty_code\validation\validation\factory\library\ConstValidationFactory;
use liberty_code\validation\validation\factory\exception\FactoryInvalidFormatException;
use liberty_code\validation\validation\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|ValidationFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|ValidationFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultValidationFactory extends DefaultFactory implements ValidationFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|ValidationFactoryInterface $objFactory = null
     */
    public function __construct(
        ValidationFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init validation factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstValidationFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstValidationFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstValidationFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstValidationFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified validation.
     * Overwrite it to set specific validation hydration.
     *
     * @param ValidationInterface $objValidation
     * @param array $tabConfigFormat
     */
    protected function hydrateValidation(ValidationInterface $objValidation, array $tabConfigFormat)
    {
        // Init var
        $tabConfig = (
            array_key_exists(ConstValidationFactory::TAB_CONFIG_KEY_CONFIG, $tabConfigFormat) ?
                $tabConfigFormat[ConstValidationFactory::TAB_CONFIG_KEY_CONFIG] :
                null
        );

        // Hydrate validation
        if(!is_null($tabConfig))
        {
            $objValidation->setConfig($tabConfig);
        }
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified validation object.
     *
     * @param ValidationInterface $objValidation
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(ValidationInterface $objValidation, array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $strValidationClassPath = $this->getStrValidationClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strValidationClassPath)) &&
            ($strValidationClassPath == get_class($objValidation))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = (
            array_key_exists(ConstValidationFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat) ?
                $tabConfigFormat[ConstValidationFactory::TAB_CONFIG_KEY_TYPE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string class path of validation,
     * from specified configured type.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrValidationClassPathFromType($strConfigType);



    /**
     * Get string class path of validation engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrValidationClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrValidationClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrValidationClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrValidationClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrValidationClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance validation,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|ValidationInterface
     */
    protected function getObjValidationNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrValidationClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance validation engine.
     *
     * @param array $tabConfigFormat
     * @param ValidationInterface $objValidation = null
     * @return null|ValidationInterface
     */
    protected function getObjValidationEngine(
        array $tabConfigFormat,
        ValidationInterface $objValidation = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objValidation = (
            is_null($objValidation) ?
                $this->getObjValidationNew($strConfigType) :
                $objValidation
        );

        // Get and hydrate validation, if required
        if(
            (!is_null($objValidation)) &&
            $this->checkConfigIsValid($objValidation, $tabConfigFormat)
        )
        {
            $this->hydrateValidation($objValidation, $tabConfigFormat);
            $result = $objValidation;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjValidation(
        array $tabConfig,
        $strConfigKey = null,
        ValidationInterface $objValidation = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjValidationEngine($tabConfigFormat, $objValidation);

        // Get validation from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjValidation($tabConfig, $strConfigKey, $objValidation);
        }

        // Return result
        return $result;
    }



}