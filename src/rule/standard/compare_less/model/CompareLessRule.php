<?php
/**
 * Description :
 * This class allows to define compare less rule class.
 * Compare less rule allows to check if specified data is less than specified compare value.
 *
 * Compare less rule uses the following specified configuration:
 * [
 *     Default rule configuration,
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by compare value"
 * ]
 *
 * Compare less rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     compare_value(required): mixed value to compare,
 *
 *     equal_enable_require(optional: got true if not found): true / false,
 * ]
 *
 * Compare less rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     compare_value(required): mixed value to compare,
 *
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by compare value"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\compare_less\model;

use liberty_code\validation\rule\model\DefaultRule;

use DateTime;
use liberty_code\library\str\library\ToolBoxString;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\compare_less\library\ConstCompareLessRule;
use liberty_code\validation\rule\standard\compare_less\exception\ValidConfigInvalidFormatException;
use liberty_code\validation\rule\standard\compare_less\exception\ErrorConfigInvalidFormatException;



class CompareLessRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check equal enable required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see CompareLessRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkEqualEnableRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            (!is_array($tabValidConfig)) ||
            (!array_key_exists(ConstCompareLessRule::TAB_VALID_CONFIG_KEY_EQUAL_ENABLE_REQUIRE, $tabValidConfig)) ||
            (intval($tabValidConfig[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_EQUAL_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $compareValue = $tabConfig[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_COMPARE_VALUE];
            $boolEqualEnable = $this->checkEqualEnableRequired($tabConfig);
            $result = (
                // Check valid value
                (
                    is_string($value) ||
                    is_numeric($value) ||
                    ($value instanceof DateTime)
                ) &&

                // Check value less (equal) than compare value
                (
                $boolEqualEnable ?
                    ($value <= $compareValue) :
                    ($value < $compareValue)
                )
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstCompareLessRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstCompareLessRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = null;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strValue = (
                ($value instanceof DateTime) ?
                    $value->format('Y-m-d H:i:s') :
                    (
                        ToolBoxString::checkConvertString($value) ?
                            strval($value) :
                            ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
                    )
            );
            $compareValue = $tabConfig[ConstCompareLessRule::TAB_ERROR_CONFIG_KEY_COMPARE_VALUE];
            $strCompareValue = (
                ($compareValue instanceof DateTime) ?
                    $compareValue->format('Y-m-d H:i:s') :
                    strval($compareValue)
            );
            $result = sprintf($strErrorMsgPattern, $strName, $strValue, $strCompareValue);
        }

        // Return result
        return $result;
    }



}