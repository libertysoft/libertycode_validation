<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\validation\validator\library\ConstValidator;
use liberty_code\validation\validator\api\ValidatorInterface;



class ToolBoxRuleConfig extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified formatted rule configuration array.
     *
     * Rule configuration array format:
     * @see ValidatorInterface::checkDataIsValid() rule configuration array format.
     *
     * Return array format:
     * [
     *     // Rule 1
     *     [
     *         rule_name(required): "string rule 1 name",
     *         rule_config(optional): [rule 1 configuration array ...]
     *     ],
     *
     *     ...,
     *
     *     // Rule N
     *     ...
     * ]
     *
     * @param array $tabRuleConfig
     * @return array
     */
    public static function getTabFormatRuleConfig(array $tabRuleConfig)
    {
        // Init var
        $result = array();

        // Run each rule configuration
        foreach($tabRuleConfig as $ruleConfig)
        {
            // Get rule name
            $strNm = (
                (is_array($ruleConfig) && isset($ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_NAME])) ?
                    $ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_NAME] :
                    (
                        (is_array($ruleConfig) && isset($ruleConfig[0])) ?
                            $ruleConfig[0] :
                            $ruleConfig
                    )
            );

            // Get rule configuration
            $tabConfig = (
                (is_array($ruleConfig) && isset($ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_CONFIG])) ?
                    $ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_CONFIG] :
                    (
                        (is_array($ruleConfig) && isset($ruleConfig[1])) ?
                            $ruleConfig[1]:
                            null
                    )
            );

            // Set formatted rule configuration, if required
            if(is_string($strNm))
            {
                // Build formatted rule configuration
                $formatRuleConfig = array(
                    ConstValidator::TAB_RULE_CONFIG_KEY_RULE_NAME => $strNm
                );

                if(is_array($tabConfig))
                {
                    $formatRuleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_CONFIG] = $tabConfig;
                }

                // Set formatted rule configuration
                $result[] = $formatRuleConfig;
            }
        }

        // Return result
        return $result;
    }



}