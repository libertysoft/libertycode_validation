<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\exception;

use Exception;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\rule\library\ConstValidatorRule;



class ValidatorInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $validator
     */
	public function __construct($validator)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstValidatorRule::EXCEPT_MSG_VALIDATOR_INVALID_FORMAT,
            mb_strimwidth(strval($validator), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified validator has valid format.
	 * 
     * @param mixed $validator
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($validator)
    {
		// Init var
		$result = (
            (!is_null($validator)) &&
			($validator instanceof ValidatorInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($validator);
		}
		
		// Return result
		return $result;
    }
	
	
	
}