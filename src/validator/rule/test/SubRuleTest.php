<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/rule/standard/test/RuleCollectionTest.php');



// Test data validation
echo('Test validation : <br />');

$tabInfo = array(
    // Test not Ko
    [
        'sub_rule_not',
        'key_1',
        'Value 1',
        null
    ],

    // Test not Ko
    [
        'sub_rule_not',
        'key_1',
        7,
        [
            'rule_config' => [
                [
                    'type_numeric',
                    'integer_only_require' => 1
                ],
                [
                    'compare_greater',
                    [
                        'compare_value' => 4,
                        'equal_enable_require' => false
                    ]
                ]
            ]
        ]
    ],

    // Test not Ok
    [
        'sub_rule_not',
        'key_1',
        3,
        [
            'rule_config' => [
                [
                    'type_numeric',
                    'integer_only_require' => 1
                ],
                [
                    'compare_greater',
                    [
                        'compare_value' => 4,
                        'equal_enable_require' => false
                    ]
                ]
            ]
        ]
    ],

    // Test not Ok
    [
        'sub_rule_not',
        'key_1',
        'Value 1',
        [
            'rule_config' => [
                [
                    'type_numeric',
                    'integer_only_require' => 1
                ],
                [
                    'compare_greater',
                    [
                        'compare_value' => 4,
                        'equal_enable_require' => false
                    ]
                ]
            ]
        ]
    ],

    // Test size Ko
    [
        'sub_rule_size',
        'key_1',
        'test',
        [
            'rule_config' => [
                [
                    'compare_greater',
                    [
                        'compare_value' => 4,
                        'equal_enable_require' => false
                    ]
                ],
                [
                    'compare_less',
                    [
                        'compare_value' => 7
                    ]
                ]
            ]
        ]
    ],

    // Test size Ko
    [
        'sub_rule_size',
        'key_1',
        ['Value 1', 'Value 2', 'Value 3', 'Value 4'],
        [
            'rule_config' => [
                [
                    'compare_greater',
                    [
                        'compare_value' => 4,
                        'equal_enable_require' => false
                    ]
                ],
                [
                    'compare_less',
                    [
                        'compare_value' => 7
                    ]
                ]
            ]
        ]
    ],

    // Test size Ok
    [
        'sub_rule_size',
        'key_1',
        5,
        [
            'rule_config' => [
                [
                    'compare_greater',
                    [
                        'compare_value' => 4,
                        'equal_enable_require' => false
                    ]
                ],
                [
                    'compare_less',
                    [
                        'compare_value' => 7
                    ]
                ]
            ]
        ]
    ],

    // Test size Ok
    [
        'sub_rule_size',
        'key_1',
        'Value 1',
        [
            'rule_config' => [
                [
                    'compare_greater',
                    [
                        'compare_value' => 4,
                        'equal_enable_require' => false
                    ]
                ],
                [
                    'compare_less',
                    [
                        'compare_value' => 7
                    ]
                ]
            ]
        ]
    ],

    // Test size Ok
    [
        'sub_rule_size',
        'key_1',
        ['Value 1', 'Value 2', 'Value 3', 'Value 4', 'Value 5'],
        [
            'rule_config' => [
                [
                    'compare_greater',
                    [
                        'compare_value' => 4,
                        'equal_enable_require' => false
                    ]
                ],
                [
                    'compare_less',
                    [
                        'compare_value' => 7
                    ]
                ]
            ]
        ]
    ],

    // Test iterate Ko
    [
        'sub_rule_iterate',
        'key_1',
        'test',
        [
            'rule_config' => [
                [
                    'type_numeric',
                    'rule_config' => ['integer_only_require' => 1]
                ],
                [
                    'compare_greater',
                    [
                        'compare_value' => 0
                    ]
                ]
            ]
        ]
    ],

    // Test iterate Ko
    [
        'sub_rule_iterate',
        'key_1',
        [
            'value 1',
            'value 2',
            'value 3'
        ],
        [
            'rule_config' => [
                [
                    'type_numeric',
                    'rule_config' => ['integer_only_require' => 1]
                ],
                [
                    'compare_greater',
                    [
                        'compare_value' => 0
                    ]
                ]
            ]
        ]
    ],

    // Test iterate Ok
    [
        'sub_rule_iterate',
        'key_1',
        [
            'value 1',
            'value 2',
            'value 3'
        ],
        [
            'rule_config' => [
                'type_string'
            ]
        ]
    ],

    // Test iterate Ok
    [
        'sub_rule_iterate',
        'key_1',
        [
            'value 1',
            'value 2',
            'value 3'
        ],
        [
            'rule_config' => [
                [
                    'type_numeric',
                    'rule_config' => ['integer_only_require' => 1]
                ],
                [
                    'compare_greater',
                    [
                        'compare_value' => 0
                    ]
                ]
            ],
            'key_valid_require' => 1
        ]
    ]
);

foreach($tabInfo as $info)
{
    echo('Test validation info: <br />');

    try{
        // Get info
        $strKey = $info[0];
        $strName = $info[1];
        $value = $info[2];
        $tabConfig = $info[3];

        // Get rule
        $boolExists = $objRuleCollection->checkExists($strKey);
        $objRule = $objRuleCollection->getObjRule($strKey);

        echo('Info: <pre>');
        print_r(array(
            $strKey,
            $strName,
            (is_object($value) ? get_class($value) : $value),
            $tabConfig
        ));
        echo('</pre>');

        // Test validation, if required
        if($boolExists)
        {
            // Get validation
            $boolIsValid = $objRule->checkIsValid($strName, $value, $tabConfig);
            $strErrorMessage = $objRule->getStrErrorMessage($strName, $value, $tabConfig);
            $objErrorException = $objRule->getObjErrorException($strName, $value, $tabConfig);

            echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
            echo('Get error message: <pre>');var_dump($strErrorMessage);echo('</pre>');
            echo('Get error exception: <pre>');
            var_dump(
                (!is_null($objErrorException)) ?
                    get_class($objErrorException) . ': ' . $objErrorException->getMessage() :
                    $objErrorException
            );
            echo('</pre>');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


