<?php
/**
 * Description :
 * This class allows to define type array rule class.
 * Type array rule allows to check if specified data is array.
 *
 * Type array rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Type array rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     index_only_require(optional: got false if not found): true / false,
 * ]
 *
 * Type array rule uses the following specified error configuration:
 * [
 *     Default rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\type_array\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\type_array\library\ConstTypeArrayRule;
use liberty_code\validation\rule\standard\type_array\exception\ValidConfigInvalidFormatException;



class TypeArrayRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check index only required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see TypeArrayRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkIndexOnlyRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            is_array($tabValidConfig) &&
            array_key_exists(ConstTypeArrayRule::TAB_VALID_CONFIG_KEY_INDEX_ONLY_REQUIRE, $tabValidConfig) &&
            (intval($tabValidConfig[ConstTypeArrayRule::TAB_VALID_CONFIG_KEY_INDEX_ONLY_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $boolIndexOnly = $this->checkIndexOnlyRequired($tabConfig);
        $result = (
            is_array($value) &&
            (
                (!$boolIndexOnly) ||
                ToolBoxTable::checkTabIsIndex($value)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstTypeArrayRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstTypeArrayRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}