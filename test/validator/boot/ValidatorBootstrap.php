<?php

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\validation\rule\model\DefaultRuleCollection;
use liberty_code\validation\rule\standard\call\model\CallableRule;
use liberty_code\validation\rule\standard\type_string\model\TypeStringRule;
use liberty_code\validation\rule\standard\type_numeric\model\TypeNumericRule;
use liberty_code\validation\rule\standard\type_boolean\model\TypeBooleanRule;
use liberty_code\validation\rule\standard\type_array\model\TypeArrayRule;
use liberty_code\validation\rule\standard\type_date\model\TypeDateRule;
use liberty_code\validation\rule\standard\type_object\model\TypeObjectRule;
use liberty_code\validation\rule\standard\is_null\model\IsNullRule;
use liberty_code\validation\rule\standard\is_empty\model\IsEmptyRule;
use liberty_code\validation\rule\standard\is_callable\model\IsCallableRule;
use liberty_code\validation\rule\standard\compare_equal\model\CompareEqualRule;
use liberty_code\validation\rule\standard\compare_less\model\CompareLessRule;
use liberty_code\validation\rule\standard\compare_greater\model\CompareGreaterRule;
use liberty_code\validation\rule\standard\compare_between\model\CompareBetweenRule;
use liberty_code\validation\rule\standard\compare_in\model\CompareInRule;
use liberty_code\validation\rule\standard\string_start\model\StringStartRule;
use liberty_code\validation\rule\standard\string_end\model\StringEndRule;
use liberty_code\validation\rule\standard\string_contain\model\StringContainRule;
use liberty_code\validation\rule\standard\string_regexp\model\StringRegexpRule;
use liberty_code\validation\rule\standard\string_date_format\model\StringDateFormatRule;
use liberty_code\validation\validator\standard\model\StandardValidator;
use liberty_code\validation\validator\rule\sub_rule\not\model\NotSubRule;
use liberty_code\validation\validator\rule\sub_rule\size\model\SizeSubRule;
use liberty_code\validation\validator\rule\sub_rule\iterate\model\IterateSubRule;
use liberty_code\validation\validator\rule\group_sub_rule\group_and\model\AndGroupSubRule;
use liberty_code\validation\validator\rule\group_sub_rule\group_or\model\OrGroupSubRule;



// Init cache repository
$objCacheRepo = new DefaultRepository(
    null,
    new DefaultTableRegister()
);

// Init rule collection and validator
$objRuleCollection = new DefaultRuleCollection();
$tabValidatorConfig = array(
    'cache_require' => true,
    'cache_key_pattern' => 'validation-%1$s-%2$s-%3$s',
    //'cache_value_is_valid_key' => 'validation-is_valid',
    //'cache_value_error_message_key' => 'validation-error_message',
    //'cache_value_error_exception_key' => 'validation-error_exception',
);
$objValidator = new StandardValidator(
    $objRuleCollection,
    $tabValidatorConfig,
    $objCacheRepo
);

// Init rules
$objCallableRule = new CallableRule();
$objTypeStringRule = new TypeStringRule();
$objTypeNumericRule = new TypeNumericRule();
$objTypeBoolRule = new TypeBooleanRule();
$objTypeArrayRule = new TypeArrayRule();
$objTypeDateRule = new TypeDateRule();
$objTypeObjRule = new TypeObjectRule();
$objIsNullRule = new IsNullRule();
$objIsEmptyRule = new IsEmptyRule();
$objIsCallRule = new IsCallableRule();
$objCompareEqualRule = new CompareEqualRule();
$objCompareLessRule = new CompareLessRule();
$objCompareGreaterRule = new CompareGreaterRule();
$objCompareBetweenRule = new CompareBetweenRule();
$objCompareInRule = new CompareInRule();
$objStringStartRule = new StringStartRule();
$objStringEndRule = new StringEndRule();
$objStringContainRule = new StringContainRule();
$objStringRegexRule = new StringRegexpRule();
$objStringDateFormatRule = new StringDateFormatRule();
$objNotSubRule = new NotSubRule($objValidator);
$objSizeSubRule = new SizeSubRule($objValidator);
$objIterateSubRule = new IterateSubRule($objValidator);
$objAndGroupSubRule = new AndGroupSubRule($objValidator);
$objOrGroupSubRule = new OrGroupSubRule($objValidator);



// Add rule
$tabRule = array(
    $objCallableRule,
    $objTypeStringRule,
    $objTypeNumericRule,
    $objTypeBoolRule,
    $objTypeArrayRule,
    $objTypeDateRule,
    $objTypeObjRule,
    $objIsNullRule,
    $objIsEmptyRule,
    $objIsCallRule,
    $objCompareEqualRule,
    $objCompareLessRule,
    $objCompareGreaterRule,
    $objCompareBetweenRule,
    $objCompareInRule,
    $objStringStartRule,
    $objStringEndRule,
    $objStringContainRule,
    $objStringRegexRule,
    $objStringDateFormatRule,
    $objNotSubRule,
    $objSizeSubRule,
    $objIterateSubRule,
    $objAndGroupSubRule,
    $objOrGroupSubRule
);
$objRuleCollection->setTabRule($tabRule);


