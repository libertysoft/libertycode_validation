<?php

namespace liberty_code\validation\rule\test;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\rule\library\ConstRule;



class BetweenRuleTest extends DefaultRule
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
		// Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Init var
        $start = (
            (isset($tabConfig['start']) && is_numeric($tabConfig['start'])) ?
                intval($tabConfig['start']) :
                null
        );

        $end = (
            (isset($tabConfig['end']) && is_numeric($tabConfig['end'])) ?
                intval($tabConfig['end']) :
                null
        );

        $result = (
            is_numeric($value) &&
            (
                (!is_numeric($start)) ||
                ($value >= $start)
            ) &&
            (
                (!is_numeric($end)) ||
                ($value <= $end)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => '%1$s must be between %2$s and %3$s'
        );
    }



    /**
     * @inheritdoc
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Init var
        $start = (
            isset($tabConfig['start']) ?
                strval($tabConfig['start']) :
                '-'
        );

        $end = (
            isset($tabConfig['end']) ?
                strval($tabConfig['end']) :
                '-'
        );

        // Return result
        return sprintf($strErrorMsgPattern, $strName, $start, $end);
    }



}