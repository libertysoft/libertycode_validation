LibertyCode_Validation
======================



Description
-----------

Library contains validation components, 
to use for data validation.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/validation ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/validation": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Rule

Rules allow to validate data, 
in specific way.

_Elements_

- Rule

    Allows to design specific validation, 
    to validate specified data.
    
- RuleCollection

    Allows to design collection of rules.
    
- CallableRule

    Extends rule features. 
    Allows to check if specified data passes specified validation callable.
    
- TypeStringRule

    Extends rule features. 
    Allows to check if specified data is valid string.
    
- TypeNumericRule

    Extends rule features. 
    Allows to check if specified data is valid numeric.

- TypeBooleanRule

    Extends rule features. 
    Allows to check if specified data is valid boolean.
    
- TypeArrayRule

    Extends rule features. 
    Allows to check if specified data is valid array.
    
- TypeDateRule

    Extends rule features. 
    Allows to check if specified data is valid date.
    
- TypeObjectRule

    Extends rule features. 
    Allows to check if specified data is valid object.
    
- IsNullRule

    Extends rule features. 
    Allows to check if specified data is null.
    
- IsEmptyRule

    Extends rule features. 
    Allows to check if specified data is empty.
    
- IsCallableRule

    Extends rule features. 
    Allows to check if specified data is callable.
    
- CompareEqualRule

    Extends rule features. 
    Allows to check if specified data is equal to specified compare value.
    
- CompareLessRule

    Extends rule features. 
    Allows to check if specified data is less than specified compare value.
  
- CompareGreaterRule

    Extends rule features. 
    Allows to check if specified data is greater than specified compare value.
      
- CompareBetweenRule

    Extends rule features. 
    Allows to check if specified data is between than specified compare values.
    
- CompareInRule

    Extends rule features. 
    Allows to check if specified data is in specified index array of compare values.
    
- StringStartRule

    Extends rule features. 
    Allows to check if specified data starts with specified start value.
    
- StringEndRule

    Extends rule features. 
    Allows to check if specified data ends with specified end value.
    
- StringContainRule

    Extends rule features. 
    Allows to check if specified data contains specified contain value.
    
- StringRegexpRule

    Extends rule features. 
    Allows to check if specified data matches with specified REGEXP pattern.
    
- StringDateFormatRule

    Extends rule features. 
    Allows to check if specified data matches with specified date format.
    
_Example_

```php
// Get rule collection
use liberty_code\validation\rule\model\DefaultCollectionRuleCollection;
$ruleCollection = new DefaultCollectionRuleCollection();
...
// Set rule
$ruleCollection->setRule(...rule object);
...
// Get rule
$rule = $ruleCollection->getObjRule(...string rule key);
...
// Check data is valid
$isValid = $rule->checkIsValid(
    '...string data name', 
    ...mixed value, 
    array(...config)
);
...
```

#### Validator

Validator allows to validate data, 
from rule configuration.

- Validator
    
    Allows to validate specified data,
    from specified rule configuration.
    
- StandardValidator

    Extends validator features. 
    Uses rule objects, to validate data.

_Example_

```php
// Get validator
use liberty_code\validation\validator\standard\model\StandardValidator;
$validator = new StandardValidator($ruleCollection);
...
// Check data is valid
$errorMessage = array();
$errorException = array();
$isValid = $validator->checkDataIsValid(
    '...string data name', 
    ...mixed value, 
    array(...rule config),
    $errorMessage,
    $errorException
);
...
// Show error messages
var_dump($errorMessage);
...
```

#### Validator rule

Validator rules use validator object, 
to validate data.

- ValidatorRule

    Extends rule features. 
    Uses validator object,
    to validate data.
    
- SubRule

    Extends validator rule features. 
    Allows to validate data, 
    from specified rule configuration.

- NotSubRule

    Extends sub-rule features. 
    Allows to check if specified data fails specified rule configuration.
    
- SizeSubRule

    Extends sub-rule features. 
    Allows to check if specified data size passes specified rule configuration.
    
- IterateSubRule

    Extends sub-rule features. 
    Allows to check if all keys or values, from specified data, 
    pass specified rule configuration.
    
- GroupSubRule

    Extends validator rule features. 
    Allows to validate data, 
    from specified rule configurations.
    
- AndGroupSubRule

    Extends group sub-rule features. 
    Allows to check if specified data 
    passes all specified rule configurations.
    
- OrGroupSubRule

    Extends group sub-rule features. 
    Allows to check if specified data 
    passes at least one of specified rule configurations.

#### Validation

Validation allows to validate specific data,
from specific rule configuration.

- Validation
    
    Contains all information, 
    to validate specific data,
    from specific rule configuration.
    
- StandardValidation

    Extends validation features. 
    Uses validator object, to validate data.

- ValidationFactory

    Allows to provide new or specified validation instance,
    from specified configuration.
    
- StandardValidationFactory

    Extends validation factory features. 
    Provides validation instance.
    
_Example_

```php
// Get validation factory
use liberty_code\validation\validation\factory\standard\model\StandardValidationFactory;
$validationFactory = new StandardValidationFactory($validator);
...
// Get new validation, from configuration
$validation = $validationFactory->getObjValidation(array(...));
...
// Check data are valid
var_dump($validation->checkDataIsValid());
...
// Show error messages
var_dump($validation->getTabErrorMessage());
...
```

---



Test
----

#### Unit test

Unit tests allows to test components features, 
and to automate their validation.

1. Requirement
    
    - Composer
    
        It requires composer installation.
        For more information: https://getcomposer.org
        
    - Command: Dependencies installation
    
        ```sh
        php composer.phar install
        ```
    
2. Command: Run unit tests
    
    ```sh
    vendor\bin\phpunit
    ```

3. Note

    It uses PHPUnit to handle unit tests.
    For more information: https://phpunit.readthedocs.io

---


