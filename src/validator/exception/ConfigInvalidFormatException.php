<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\exception;

use Exception;

use liberty_code\validation\validator\library\ConstValidator;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstValidator::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid cache required option
            (
                (!isset($config[ConstValidator::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstValidator::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    is_int($config[ConstValidator::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    (
                        is_string($config[ConstValidator::TAB_CONFIG_KEY_CACHE_REQUIRE]) &&
                        ctype_digit($config[ConstValidator::TAB_CONFIG_KEY_CACHE_REQUIRE])
                    )
                )
            ) &&

            // Check valid cache key pattern
            (
                (!isset($config[ConstValidator::TAB_CONFIG_KEY_CACHE_KEY_PATTERN])) ||
                (
                    is_string($config[ConstValidator::TAB_CONFIG_KEY_CACHE_KEY_PATTERN]) &&
                    (trim($config[ConstValidator::TAB_CONFIG_KEY_CACHE_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid cache value is valid key
            (
                (!isset($config[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_IS_VALID_KEY])) ||
                (
                    is_string($config[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_IS_VALID_KEY]) &&
                    (trim($config[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_IS_VALID_KEY]) != '')
                )
            ) &&

            // Check valid cache value error message key
            (
                (!isset($config[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_MESSAGE_KEY])) ||
                (
                    is_string($config[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_MESSAGE_KEY]) &&
                    (trim($config[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_MESSAGE_KEY]) != '')
                )
            ) &&

            // Check valid cache value error exception key
            (
                (!isset($config[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_EXCEPTION_KEY])) ||
                (
                    is_string($config[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_EXCEPTION_KEY]) &&
                    (trim($config[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_EXCEPTION_KEY]) != '')
                )
            ) &&

            // Check valid cache set configuration
            (
                (!isset($config[ConstValidator::TAB_CONFIG_KEY_CACHE_SET_CONFIG])) ||
                is_array($config[ConstValidator::TAB_CONFIG_KEY_CACHE_SET_CONFIG])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}