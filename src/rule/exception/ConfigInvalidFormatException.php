<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\exception;

use Exception;

use liberty_code\validation\rule\library\ConstRule;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRule::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid key
            (
                (!isset($config[ConstRule::TAB_CONFIG_KEY_KEY])) ||
                (
                    is_string($config[ConstRule::TAB_CONFIG_KEY_KEY]) &&
                    (trim($config[ConstRule::TAB_CONFIG_KEY_KEY]) != '')
                )
            ) &&

            // Check valid error message pattern
            (
                (!isset($config[ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN])) ||
                (
                    is_string($config[ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN]) &&
                    (trim($config[ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN]) != '')
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @param array $tabFixConfig
     * @return boolean
     * @throws static
     */
    public static function setCheck($config, array $tabFixConfig)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&
            ($tabFixConfig === array_intersect_key($config, $tabFixConfig)) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}