<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/rule/standard/test/RuleCollectionTest.php');



// Test data validation
echo('Test validation : <br />');

$tabInfo = array(
    // Test start Ko
    [
        'string_start',
        'key_1',
        'Value 1',
        null
    ],

    // Test start Ko
    [
        'string_start',
        'key_1',
        'value 1',
        ['start_value' => 'Val']
    ],

    // Test start Ok
    [
        'string_start',
        'key_1',
        'Value 1',
        ['start_value' => 'Val']
    ],

    // Test end Ko
    [
        'string_end',
        'key_2',
        'Value 2',
        null
    ],

    // Test end Ko
    [
        'string_end',
        'key_2',
        7,
        ['end_value' => ' 2']
    ],

    // Test end Ok
    [
        'string_end',
        'key_2',
        'Value 2',
        ['end_value' => ' 2']
    ],

    // Test contain Ko
    [
        'string_contain',
        'key_1',
        'ValuE 1',
        ['contain_value' => 'lue']
    ],

    // Test contain Ok
    [
        'string_contain',
        'key_1',
        'Value 1',
        ['contain_value' => 'lue']
    ],

    // Test regexp Ko
    [
        'string_regexp',
        'key_2',
        'Value 22',
        ['regexp' => '#^[^\d]*\d$#']
    ],

    // Test regexp Ok
    [
        'string_regexp',
        'key_2',
        'Value 2',
        ['regexp' => '#^[^\d]*\d$#']
    ],

    // Test date format Ko
    [
        'string_date_format',
        'key_1',
        'Value 1',
        ['format' => 'Y-m-d']
    ],

    // Test date format Ko
    [
        'string_date_format',
        'key_1',
        '2019-01-27 20:58:37',
        ['format' => 'Y-m-d']
    ],

    // Test date format Ok
    [
        'string_date_format',
        'key_1',
        '2019-01-27',
        ['format' => 'Y-m-d']
    ],

    // Test date format Ko
    [
        'string_date_format',
        'key_2',
        'Value 2',
        ['format' => 'Y-m-d H:i:s']
    ],

    // Test date format Ko
    [
        'string_date_format',
        'key_2',
        '2019-01-27',
        ['format' => 'Y-m-d H:i:s']
    ],

    // Test date format Ok
    [
        'string_date_format',
        'key_1',
        '2019-01-27 20:58:37',
        ['format' => 'Y-m-d H:i:s']
    ]
);

foreach($tabInfo as $info)
{
    echo('Test validation info: <br />');

    try{
        // Get info
        $strKey = $info[0];
        $strName = $info[1];
        $value = $info[2];
        $tabConfig = $info[3];

        // Get rule
        $boolExists = $objRuleCollection->checkExists($strKey);
        $objRule = $objRuleCollection->getObjRule($strKey);

        echo('Info: <pre>');
        print_r(array(
            $strKey,
            $strName,
            (is_object($value) ? get_class($value) : $value),
            $tabConfig
        ));
        echo('</pre>');

        // Test validation, if required
        if($boolExists)
        {
            // Get validation
            $boolIsValid = $objRule->checkIsValid($strName, $value, $tabConfig);
            $strErrorMessage = $objRule->getStrErrorMessage($strName, $value, $tabConfig);
            $objErrorException = $objRule->getObjErrorException($strName, $value, $tabConfig);

            echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
            echo('Get error message: <pre>');var_dump($strErrorMessage);echo('</pre>');
            echo('Get error exception: <pre>');
            var_dump(
                (!is_null($objErrorException)) ?
                    get_class($objErrorException) . ': ' . $objErrorException->getMessage() :
                    $objErrorException
            );
            echo('</pre>');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


