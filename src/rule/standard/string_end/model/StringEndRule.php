<?php
/**
 * Description :
 * This class allows to define string end rule class.
 * String end rule allows to check if specified data ends with specified end value.
 *
 * String end rule uses the following specified configuration:
 * [
 *     Default rule configuration,
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by end value"
 * ]
 *
 * String end rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     end_value(required): string end value,
 *
 *     encoding_name(optional: got internal encoding name if not found): string encoding name
 * ]
 *
 * String end rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     end_value(required): string end value,
 *
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by end value"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\string_end\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\string_end\library\ConstStringEndRule;
use liberty_code\validation\rule\standard\string_end\exception\ValidConfigInvalidFormatException;
use liberty_code\validation\rule\standard\string_end\exception\ErrorConfigInvalidFormatException;



class StringEndRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strEndValue = $tabConfig[ConstStringEndRule::TAB_VALID_CONFIG_KEY_END_VALUE];
            $strEncodingName = $this->getStrEncodingName($tabConfig);

            // Check value ends with end value
            $result = (
                is_string($value) &&
                ToolBoxString::checkEndsWith($value, $strEndValue, $strEncodingName)
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstStringEndRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstStringEndRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * Get encoding name.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see StringEndRule
     *
     * @param array $tabConfig = null
     * @return null|string
     */
    protected function getStrEncodingName(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            isset($tabValidConfig[ConstStringEndRule::TAB_VALID_CONFIG_KEY_ENCODING_NAME]) ?
                $tabValidConfig[ConstStringEndRule::TAB_VALID_CONFIG_KEY_ENCODING_NAME] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = null;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strValue = (
                ToolBoxString::checkConvertString($value) ?
                    strval($value) :
                    ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
            );
            $strEndValue = $tabConfig[ConstStringEndRule::TAB_ERROR_CONFIG_KEY_END_VALUE];
            $result = sprintf($strErrorMsgPattern, $strName, $strValue, $strEndValue);
        }

        // Return result
        return $result;
    }



}