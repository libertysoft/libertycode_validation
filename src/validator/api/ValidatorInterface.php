<?php
/**
 * Description :
 * This class allows to describe behavior of validator class.
 * Validator allows to validate specified data,
 * from specified rule configuration.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\api;



interface ValidatorInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified data is valid,
     * from specified rule configuration.
     * Errors can be returned,
     * with all failed rules names.
     *
     * Rule configuration array format:
     * [
     *     // Rule 1
     *     [
     *         rule_name(required): "string rule 1 name",
     *         rule_config(optional): [rule 1 configuration array ...]
     *     ],
     *
     *     OR
     *
     *     [
     *         "string rule 1 name (required)",
     *         [rule 1 configuration array ... (optional)]
     *     ],
     *
     *     OR
     *
     *     "string rule 1 name",
     *
     *     ...,
     *
     *     // Rule N
     *     ...
     * ]
     *
     * Error messages array format:
     * [key: rule name => value: error: null|string message]
     *
     * Error exceptions array format:
     * [key: rule name => value: error: null|object exception]
     *
     * @param string $strName
     * @param mixed $value
     * @param array $tabRuleConfig
     * @param array &$tabErrorMessage = array()
     * @param array &$tabErrorException = array()
     * @return boolean
     */
    public function checkDataIsValid(
        $strName,
        $value,
        array $tabRuleConfig,
        array &$tabErrorMessage = array(),
        array &$tabErrorException = array()
    );



    /**
     * Check if specified data are valid,
     * from specified rule configurations.
     * Errors can be provided,
     * with all failed data and failed rules names.
     *
     * Data array format:
     * [key: data name => value: data value]
     *
     * Rule configurations array format:
     * [key: data name => value: @see checkDataIsValid() rule configuration array format]
     *
     * Error messages array format:
     * [key: data name => value: @see checkDataIsValid() error messages array format]
     *
     * Error exceptions array format:
     * [key: data name => value: @see checkDataIsValid() error exceptions array format]
     *
     * @param array $tabData
     * @param array $tabRuleConfig
     * @param array &$tabErrorMessage = array()
     * @param array &$tabErrorException = array()
     * @return boolean
     */
    public function checkTabDataIsValid(
        array $tabData,
        array $tabRuleConfig,
        array &$tabErrorMessage = array(),
        array &$tabErrorException = array()
    );
}