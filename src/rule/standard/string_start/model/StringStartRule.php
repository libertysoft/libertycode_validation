<?php
/**
 * Description :
 * This class allows to define string start rule class.
 * String start rule allows to check if specified data starts with specified start value.
 *
 * String start rule uses the following specified configuration:
 * [
 *     Default rule configuration,
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by start value"
 * ]
 *
 * String start rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     start_value(required): string start value,
 *
 *     encoding_name(optional: got internal encoding name if not found): string encoding name
 * ]
 *
 * String start rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     start_value(required): string start value,
 *
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by start value"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\string_start\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\string_start\library\ConstStringStartRule;
use liberty_code\validation\rule\standard\string_start\exception\ValidConfigInvalidFormatException;
use liberty_code\validation\rule\standard\string_start\exception\ErrorConfigInvalidFormatException;



class StringStartRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strStartValue = $tabConfig[ConstStringStartRule::TAB_VALID_CONFIG_KEY_START_VALUE];
            $strEncodingName = $this->getStrEncodingName($tabConfig);

            // Check value starts with start value
            $result = (
                is_string($value) &&
                ToolBoxString::checkStartsWith($value, $strStartValue, $strEncodingName)
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstStringStartRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstStringStartRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * Get encoding name.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see StringStartRule
     *
     * @param array $tabConfig = null
     * @return null|string
     */
    protected function getStrEncodingName(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            isset($tabValidConfig[ConstStringStartRule::TAB_VALID_CONFIG_KEY_ENCODING_NAME]) ?
                $tabValidConfig[ConstStringStartRule::TAB_VALID_CONFIG_KEY_ENCODING_NAME] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = null;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strValue = (
                ToolBoxString::checkConvertString($value) ?
                    strval($value) :
                    ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
            );
            $strStartValue = $tabConfig[ConstStringStartRule::TAB_ERROR_CONFIG_KEY_START_VALUE];
            $result = sprintf($strErrorMsgPattern, $strName, $strValue, $strStartValue);
        }

        // Return result
        return $result;
    }



}