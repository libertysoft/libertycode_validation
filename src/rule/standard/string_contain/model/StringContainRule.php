<?php
/**
 * Description :
 * This class allows to define string contain rule class.
 * String contain rule allows to check if specified data contains specified contain value.
 *
 * String contain rule uses the following specified configuration:
 * [
 *     Default rule configuration,
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by contain value"
 * ]
 *
 * String contain rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     contain_value(required): string contain value,
 *
 *     encoding_name(optional: got internal encoding name if not found): string encoding name
 * ]
 *
 * String contain rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     contain_value(required): string contain value,
 *
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by contain value"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\string_contain\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\string_contain\library\ConstStringContainRule;
use liberty_code\validation\rule\standard\string_contain\exception\ValidConfigInvalidFormatException;
use liberty_code\validation\rule\standard\string_contain\exception\ErrorConfigInvalidFormatException;



class StringContainRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strContainValue = $tabConfig[ConstStringContainRule::TAB_VALID_CONFIG_KEY_CONTAIN_VALUE];
            $strEncodingName = $this->getStrEncodingName($tabConfig);

            // Check value contains contain value
            $result = (
                is_string($value) &&
                ToolBoxString::checkContains($value, $strContainValue, $strEncodingName)
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstStringContainRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstStringContainRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * Get encoding name.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see StringEndRule
     *
     * @param array $tabConfig = null
     * @return null|string
     */
    protected function getStrEncodingName(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            isset($tabValidConfig[ConstStringContainRule::TAB_VALID_CONFIG_KEY_ENCODING_NAME]) ?
                $tabValidConfig[ConstStringContainRule::TAB_VALID_CONFIG_KEY_ENCODING_NAME] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = null;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strValue = (
                ToolBoxString::checkConvertString($value) ?
                    strval($value) :
                    ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
            );
            $strContainValue = $tabConfig[ConstStringContainRule::TAB_ERROR_CONFIG_KEY_CONTAIN_VALUE];
            $result = sprintf($strErrorMsgPattern, $strName, $strValue, $strContainValue);
        }

        // Return result
        return $result;
    }



}