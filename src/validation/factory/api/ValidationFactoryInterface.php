<?php
/**
 * Description :
 * This class allows to describe behavior of validation factory class.
 * Validation factory allows to provide new or specified validation instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined validation types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\factory\api;

use liberty_code\validation\validation\api\ValidationInterface;



interface ValidationFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of validation,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrValidationClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified validation object,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param ValidationInterface $objValidation = null
     * @return null|ValidationInterface
     */
    public function getObjValidation(
        array $tabConfig,
        $strConfigKey = null,
        ValidationInterface $objValidation = null
    );



}