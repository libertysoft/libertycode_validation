<?php
/**
 * Description :
 * This class allows to define standard validation factory class.
 * Standard validation factory allows to provide and hydrate validation instance.
 *
 * Standard validation factory uses the following specified configuration, to get and hydrate validation:
 * [
 *     Default validation factory configuration,
 *
 *     type(optional): "default",
 *
 *     config(optional): [
 *         ... specific @see StandardValidation configuration array format
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\factory\standard\model;

use liberty_code\validation\validation\factory\model\DefaultValidationFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validation\standard\exception\ValidatorInvalidFormatException;
use liberty_code\validation\validation\standard\model\StandardValidation;
use liberty_code\validation\validation\factory\api\ValidationFactoryInterface;
use liberty_code\validation\validation\factory\standard\library\ConstStandardValidationFactory;



/**
 * @method ValidatorInterface getObjValidator() Get validator object.
 * @method void setObjValidator(ValidatorInterface $objValidator) Set validator object.
 */
class StandardValidationFactory extends DefaultValidationFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ValidatorInterface $objValidator
     */
    public function __construct(
        ValidatorInterface $objValidator,
        ValidationFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objFactory,
            $objProvider
        );

        // Init validator
        $this->setObjValidator($objValidator);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStandardValidationFactory::DATA_KEY_DEFAULT_VALIDATOR))
        {
            $this->__beanTabData[ConstStandardValidationFactory::DATA_KEY_DEFAULT_VALIDATOR] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStandardValidationFactory::DATA_KEY_DEFAULT_VALIDATOR
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStandardValidationFactory::DATA_KEY_DEFAULT_VALIDATOR:
                    ValidatorInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrValidationClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of validation, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardValidationFactory::CONFIG_TYPE_STANDARD:
                $result = StandardValidation::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjValidationNew($strConfigType)
    {
        // Init var
        $result = null;
        $objValidator = $this->getObjValidator();

        // Get validation, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardValidationFactory::CONFIG_TYPE_STANDARD:
                $result = new StandardValidation($objValidator);
                break;
        }

        // Return result
        return $result;
    }



}