<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\test\feature;

use PHPUnit\Framework\TestCase;

use liberty_code\validation\validator\standard\model\StandardValidator;
use liberty_code\validation\validation\standard\model\StandardValidation;
use liberty_code\validation\validation\factory\exception\ConfigInvalidFormatException;
use liberty_code\validation\validation\factory\api\ValidationFactoryInterface;
use liberty_code\validation\validation\factory\model\DefaultValidationFactory;
use liberty_code\validation\validation\factory\standard\model\StandardValidationFactory;



/**
 * @cover ValidationFactoryInterface
 * @cover DefaultValidationFactory
 * @cover StandardValidationFactory
 */
class ValidationFactoryTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can create validation object.
     *
     * @param null|string $strConfigKey
     * @param array $tabConfig
     * @param null|array $tabInitialConfig
     * @param string|array $expectResult
     * @dataProvider providerCreateValidation
     */
    public function testCanCreateValidation(
        $strConfigKey,
        array $tabConfig,
        $tabInitialConfig,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/validation/factory/boot/ValidationFactoryBootstrap.php');
        /**
         * @var StandardValidator $objValidator
         * @var StandardValidationFactory $objValidationFactory
         */

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Init initial validation
        $objInitialValidation = (
            (!is_null($tabInitialConfig)) ?
                new StandardValidation($objValidator, $tabInitialConfig) :
                null
        );

        // Get validation
        $objValidation = $objValidationFactory->getObjValidation($tabConfig, $strConfigKey, $objInitialValidation);
        $strClassPath = $objValidationFactory->getStrValidationClassPath($tabConfig, $strConfigKey);

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $strExpectClassPath = $expectResult[0];

            // Set assertions (check validation creation)
            if(is_null($objValidation))
            {
                $this->assertSame(null, $objValidation);
                $this->assertSame($strExpectClassPath, $strClassPath);
            }
            else
            {
                $this->assertInstanceOf($strClassPath, $objValidation);
                $this->assertSame($strExpectClassPath, get_class($objValidation));
            }

            // Print
            /*
            echo('Get validation class path (from factory): '. PHP_EOL);var_dump($strClassPath);echo(PHP_EOL);
            //*/

            // Set assertions, if required
            if(!is_null($objValidation))
            {
                // Get info
                $tabConfig = $objValidation->getTabConfig();
                $tabData = $objValidation->getTabData();
                $tabRuleConfig = $objValidation->getTabRuleConfig();
                $tabExpectConfig = $expectResult[1];
                $tabExpectData = $expectResult[2];
                $tabExpectRuleConfig = $expectResult[3];

                // Set assertions (check validation details)
                $this->assertEquals($tabExpectConfig, $tabConfig);
                $this->assertEquals($tabExpectData, $tabData);
                $this->assertEquals($tabExpectRuleConfig, $tabRuleConfig);

                // Print
                /*
                echo('Get validation class path (from object): '. PHP_EOL);var_dump(get_class($objValidation));echo(PHP_EOL);
                echo('Get validation config: ' . PHP_EOL);var_dump($tabConfig);echo(PHP_EOL);
                echo('Get validation data: ' . PHP_EOL);var_dump($tabData);echo(PHP_EOL);
                echo('Get validation rule configurations: ' . PHP_EOL);var_dump($tabRuleConfig);echo(PHP_EOL);
                //*/
            }
        }
    }



    /**
     * Data provider,
     * to test can create validation object.
     *
     * @return array
     */
    public function providerCreateValidation()
    {
        // Init var
        $tabData = array(
            'data-key-1' => 'Value 1',
            'data-key-2' => 7,
            'data-key-3' => true,
            'data-key-4' => null
        );
        $tabRuleConfig = array(
            'data-key-1' => [
                'type_string',
                [
                    'string_start',
                    ['start_value' => 'Val']
                ],
                [
                    'string_end',
                    ['end_value' => ' 1']
                ]
            ],
            'data-key-2' => [
                [
                    'type_numeric',
                    ['integer_only_require' => 1]
                ],
                [
                    'compare_greater',
                    ['compare_value' => 0]
                ]
            ],
            'data-key-3' => ['type_boolean']
        );
        $tabConfig = array(
            'data' => $tabData,
            'rule_config' => $tabRuleConfig
        );

        // Return result
        return array(
            'Create validation: fail to create standard validation (invalid type format)' => [
                null,
                [
                    'type' => 7,
                    'config' => $tabConfig
                ],
                null,
                ConfigInvalidFormatException::class
            ],
            'Create validation: fail to create standard validation (type not found)' => [
                null,
                [
                    'type' => 'test',
                    'config' => $tabConfig
                ],
                null,
                [
                    null
                ]
            ],
            'Create validation: fail to create standard validation (invalid config format)' => [
                null,
                [
                    'type' => 'standard',
                    'config' => 7
                ],
                null,
                ConfigInvalidFormatException::class
            ],
            'Create validation: success to create standard validation (without type, initial validation)' => [
                null,
                [
                    'config' => $tabConfig
                ],
                null,
                [
                    StandardValidation::class,
                    $tabConfig,
                    $tabData,
                    $tabRuleConfig
                ]
            ],
            'Create validation: success to create standard validation (with type; without initial validation)' => [
                null,
                [
                    'type' => 'standard',
                    'config' => $tabConfig
                ],
                null,
                [
                    StandardValidation::class,
                    $tabConfig,
                    $tabData,
                    $tabRuleConfig
                ]
            ],
            'Create validation: success to create standard validation (with type, initial validation)' => [
                null,
                [
                    'type' => 'standard',
                    'config' => $tabConfig
                ],
                [
                    'data' => $tabData
                ],
                [
                    StandardValidation::class,
                    $tabConfig,
                    $tabData,
                    $tabRuleConfig
                ]
            ]
        );
    }



}