<?php
/**
 * Description :
 * This class allows to define callable rule class.
 * Callable rule allows to check if specified data passes specified validation callable.
 *
 * Callable rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Callable rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     valid_callable(required):
 *     Check data validation callback function: boolean function(string $strName, mixed $value)
 * ]
 *
 * Callable rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     error_message_callable(optional):
 *     Get string error message callback function: null|string function(string $strName, mixed $value)
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\call\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\call\library\ConstCallableRule;
use liberty_code\validation\rule\standard\call\exception\ValidConfigInvalidFormatException;
use liberty_code\validation\rule\standard\call\exception\ErrorConfigInvalidFormatException;



class CallableRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $checkIsValid = $tabConfig[ConstCallableRule::TAB_VALID_CONFIG_KEY_VALID_CALLABLE];
            $result = boolval($checkIsValid($strName, $value));
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstCallableRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstCallableRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    public function getStrErrorMessage(
        $strName,
        $value,
        array $tabConfig = null
    )
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $getStrErrorMessage = (
            isset($tabConfig[ConstCallableRule::TAB_ERROR_CONFIG_KEY_ERROR_MESSAGE_CALLABLE]) ?
                $tabConfig[ConstCallableRule::TAB_ERROR_CONFIG_KEY_ERROR_MESSAGE_CALLABLE] :
                null
        );
        $result = (
            is_callable($getStrErrorMessage) ?
                $getStrErrorMessage($strName, $value) :
                parent::getStrErrorMessage($strName, $value, $tabConfig)
        );

        // Return result
        return $result;
    }



}