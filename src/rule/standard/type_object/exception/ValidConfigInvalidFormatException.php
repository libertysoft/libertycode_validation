<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\type_object\exception;

use Exception;

use liberty_code\validation\rule\standard\type_object\library\ConstTypeObjectRule;



class ValidConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstTypeObjectRule::EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function($tabStr)
        {
            $result = is_array($tabStr);

            // Check each column name is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid class paths array
            (
                (!isset($config[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_CLASS_PATH])) ||
                (
                    is_array($config[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_CLASS_PATH]) &&
                    $checkTabStrIsValid($config[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_CLASS_PATH])
                )
            ) &&

            // Check valid integer only required option
            (
                (!isset($config[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_EXTEND_ENABLE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_EXTEND_ENABLE_REQUIRE]) ||
                    is_int($config[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_EXTEND_ENABLE_REQUIRE]) ||
                    (
                        is_string($config[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_EXTEND_ENABLE_REQUIRE]) &&
                        ctype_digit($config[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_EXTEND_ENABLE_REQUIRE])
                    )
                )
            ) ;

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}