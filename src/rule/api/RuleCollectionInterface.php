<?php
/**
 * Description :
 * This class allows to describe behavior of rules collection class.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\api;

use liberty_code\validation\rule\api\RuleInterface;



interface RuleCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

    /**
     * Check if specified rule exists.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get index array of rule keys.
     *
     * @return array
     */
    public function getTabKey();



    /**
     * Get rule,
     * from specified key.
     *
     * @param string $strKey
     * @return null|RuleInterface
     */
    public function getObjRule($strKey);

	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set specified rule,
     * and return its key.
     *
     * @param RuleInterface $objRule
     * @return string
     */
    public function setRule(RuleInterface $objRule);



    /**
     * Set specified rules (index array or collection),
     * and return its index array of keys.
     *
     * @param array|static $tabRule
     * @return array
     */
    public function setTabRule($tabRule);



    /**
     * Remove specified rule,
     * and return its instance.
     *
     * @param string $strKey
     * @return RuleInterface
     */
    public function removeRule($strKey);



    /**
     * Remove all rules.
     */
    public function removeRuleAll();
}