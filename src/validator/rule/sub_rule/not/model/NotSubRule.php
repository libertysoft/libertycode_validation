<?php
/**
 * Description :
 * This class allows to define not sub-rule class.
 * Not sub-rule allows to check if specified data fails specified rule configuration.
 *
 * Not sub-rule uses the following specified configuration:
 * [
 *     Sub-rule configuration
 * ]
 *
 * Not sub-rule uses the following specified validation configuration:
 * [
 *     Sub-rule validation configuration
 * ]
 *
 * Not sub-rule uses the following specified error configuration:
 * [
 *     Sub-rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\sub_rule\not\model;

use liberty_code\validation\validator\rule\sub_rule\model\SubRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\rule\sub_rule\not\library\ConstNotSubRule;



class NotSubRule extends SubRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(ValidatorInterface $objValidator)
    {
        // Call parent constructor
        parent::__construct($objValidator);
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabRuleConfig,
        array $tabConfig
    )
    {
        // Init var
        $objValidator = $this->getObjValidator();
        $result = (!$objValidator->checkDataIsValid($strName, $value, $tabRuleConfig));

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstNotSubRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstNotSubRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}