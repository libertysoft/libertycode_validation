<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\compare_between\library;



class ConstCompareBetweenRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'compare_between';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s must be between %3$s and %4$s.';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_GREATER_COMPARE_VALUE = 'greater_compare_value';
    const TAB_VALID_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE = 'greater_equal_enable_require';
    const TAB_VALID_CONFIG_KEY_LESS_COMPARE_VALUE = 'less_compare_value';
    const TAB_VALID_CONFIG_KEY_LESS_EQUAL_ENABLE_REQUIRE = 'less_equal_enable_require';

    // Error configuration
    const TAB_ERROR_CONFIG_KEY_GREATER_COMPARE_VALUE = 'greater_compare_value';
    const TAB_ERROR_CONFIG_KEY_LESS_COMPARE_VALUE = 'less_compare_value';


	
    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the compare between rule validation configuration standard.';
    const EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the compare between rule error configuration standard.';



}