<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\group_sub_rule\exception;

use Exception;

use liberty_code\validation\validator\rule\group_sub_rule\library\ConstGroupSubRule;



class ValidConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstGroupSubRule::EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init rule configurations associative array check function
        $checkTabRuleConfigIsValid = function(array $tabRuleConfig)
        {
            $result = (
                is_array($tabRuleConfig) &&
                (count($tabRuleConfig) > 0)
            );

            // Check each rule configuration is valid, if required
            if($result)
            {
                $tabRuleConfigNm = array_keys($tabRuleConfig);
                for($intCpt = 0; ($intCpt < count($tabRuleConfigNm)) && $result; $intCpt++)
                {
                    $strRuleConfigNm = $tabRuleConfigNm[$intCpt];
                    $ruleConfig = $tabRuleConfig[$strRuleConfigNm];
                    $result = (
                        is_string($strRuleConfigNm) &&
                        (trim($strRuleConfigNm) != '') &&
                        is_array($ruleConfig)
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid rule configuration array
            array_key_exists(ConstGroupSubRule::TAB_VALID_CONFIG_KEY_RULE_CONFIG, $config) &&
            $checkTabRuleConfigIsValid($config[ConstGroupSubRule::TAB_VALID_CONFIG_KEY_RULE_CONFIG]);

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}