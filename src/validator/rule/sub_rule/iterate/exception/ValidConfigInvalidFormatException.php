<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\sub_rule\iterate\exception;

use Exception;

use liberty_code\validation\validator\rule\sub_rule\iterate\library\ConstIterateSubRule;



class ValidConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstIterateSubRule::EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid key validation required option
            (
                (!isset($config[ConstIterateSubRule::TAB_VALID_CONFIG_KEY_KEY_VALID_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstIterateSubRule::TAB_VALID_CONFIG_KEY_KEY_VALID_REQUIRE]) ||
                    is_int($config[ConstIterateSubRule::TAB_VALID_CONFIG_KEY_KEY_VALID_REQUIRE]) ||
                    (
                        is_string($config[ConstIterateSubRule::TAB_VALID_CONFIG_KEY_KEY_VALID_REQUIRE]) &&
                        ctype_digit($config[ConstIterateSubRule::TAB_VALID_CONFIG_KEY_KEY_VALID_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}