<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\group_sub_rule\group_or\library;



class ConstOrGroupSubRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'group_sub_rule_or';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s must pass at least one of following groups of rules: %3$s.';



}