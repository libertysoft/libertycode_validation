<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\factory\library;



class ConstValidationFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_FACTORY = 'objFactory';



    // Configuration
    const TAB_CONFIG_KEY_TYPE = 'type';
    const TAB_CONFIG_KEY_CONFIG = 'config';



    // Exception message constants
    const EXCEPT_MSG_FACTORY_INVALID_FORMAT = 'Following validation factory "%1$s" invalid! It must be null or a validation factory object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default validation factory configuration standard.';



}