<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\compare_between\exception;

use Exception;

use DateTime;
use liberty_code\validation\rule\standard\compare_between\library\ConstCompareBetweenRule;



class ErrorConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCompareBetweenRule::EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid greater compare value
            array_key_exists(ConstCompareBetweenRule::TAB_ERROR_CONFIG_KEY_GREATER_COMPARE_VALUE, $config) &&
            (
                is_string($config[ConstCompareBetweenRule::TAB_ERROR_CONFIG_KEY_GREATER_COMPARE_VALUE]) ||
                is_numeric($config[ConstCompareBetweenRule::TAB_ERROR_CONFIG_KEY_GREATER_COMPARE_VALUE]) ||
                ($config[ConstCompareBetweenRule::TAB_ERROR_CONFIG_KEY_GREATER_COMPARE_VALUE] instanceof DateTime)
            ) &&

            // Check valid less compare value
            array_key_exists(ConstCompareBetweenRule::TAB_ERROR_CONFIG_KEY_LESS_COMPARE_VALUE, $config) &&
            (
                is_string($config[ConstCompareBetweenRule::TAB_ERROR_CONFIG_KEY_LESS_COMPARE_VALUE]) ||
                is_numeric($config[ConstCompareBetweenRule::TAB_ERROR_CONFIG_KEY_LESS_COMPARE_VALUE]) ||
                ($config[ConstCompareBetweenRule::TAB_ERROR_CONFIG_KEY_LESS_COMPARE_VALUE] instanceof DateTime)
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}