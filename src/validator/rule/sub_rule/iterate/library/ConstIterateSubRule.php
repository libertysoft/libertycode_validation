<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\sub_rule\iterate\library;



class ConstIterateSubRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'sub_rule_iterate';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s must be a valid array or iterator (passes following rules: %3$s).';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_KEY_VALID_REQUIRE = 'key_valid_require';



    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the iterate sub-rule validation configuration standard.';



}