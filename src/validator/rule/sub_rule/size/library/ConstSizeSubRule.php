<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\sub_rule\size\library;



class ConstSizeSubRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'sub_rule_size';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s must have valid size (passes following rules: %3$s).';



}