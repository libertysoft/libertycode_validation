<?php
/**
 * Description :
 * This class allows to define iterate sub-rule class.
 * Iterate sub-rule allows to check if all keys or values, from specified array or iterator data,
 * pass specified rule configuration.
 *
 * Iterate sub-rule uses the following specified configuration:
 * [
 *     Sub-rule configuration
 * ]
 *
 * Iterate sub-rule uses the following specified validation configuration:
 * [
 *     Sub-rule validation configuration,
 *
 *     key_valid_require(optional: got false if not found): true / false
 * ]
 *
 * Iterate sub-rule uses the following specified error configuration:
 * [
 *     Sub-rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\sub_rule\iterate\model;

use liberty_code\validation\validator\rule\sub_rule\model\SubRule;

use Iterator;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\rule\sub_rule\iterate\library\ConstIterateSubRule;
use liberty_code\validation\validator\rule\sub_rule\iterate\exception\ValidConfigInvalidFormatException;



class IterateSubRule extends SubRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(ValidatorInterface $objValidator)
    {
        // Call parent constructor
        parent::__construct($objValidator);
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check key validation required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see IterateSubRule
     *
     * @param array $tabConfig
     * @return boolean
     */
    protected function checkKeyValidRequired(array $tabConfig)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            array_key_exists(ConstIterateSubRule::TAB_VALID_CONFIG_KEY_KEY_VALID_REQUIRE, $tabValidConfig) &&
            (intval($tabValidConfig[ConstIterateSubRule::TAB_VALID_CONFIG_KEY_KEY_VALID_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabRuleConfig,
        array $tabConfig
    )
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $objValidator = $this->getObjValidator();
        $boolKeyValidRequired = $this->checkKeyValidRequired($tabConfig);
        $result = (
            ($value instanceof Iterator) ||
            is_array($value)
        );

        // Run each key and value from data, if required
        if($result)
        {
            foreach($value as $valKey => $valValue)
            {
                $result = $objValidator->checkDataIsValid(
                    $strName,
                    ($boolKeyValidRequired ? $valKey : $valValue),
                    $tabRuleConfig
                );

                if(!$result) break;
            }
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstIterateSubRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstIterateSubRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}