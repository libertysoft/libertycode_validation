<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\test\feature\validator;

use PHPUnit\Framework\TestCase;

use Exception;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\validation\rule\exception\ValidationFailException;
use liberty_code\validation\rule\model\DefaultRuleCollection;
use liberty_code\validation\validator\exception\DataNameInvalidFormatException;
use liberty_code\validation\validator\exception\RuleConfigInvalidFormatException;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\model\DefaultValidator;
use liberty_code\validation\validator\standard\exception\RuleNotFoundException;
use liberty_code\validation\validator\standard\model\StandardValidator;



/**
 * @cover ValidatorInterface
 * @cover DefaultValidator
 * @cover StandardValidator
 */
class ValidatorTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can check if data is valid.
     *
     * @param string $strName
     * @param mixed $value
     * @param array $tabRuleConfig
     * @param boolean $boolCacheRequired
     * @param string|array $expectResult
     * @dataProvider providerCheckDataIsValid
     */
    public function testCanCheckDataIsValid(
        $strName,
        $value,
        array $tabRuleConfig,
        $boolCacheRequired,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/validator/boot/ValidatorBootstrap.php');
        /**
         * @var DefaultRepository $objCacheRepo
         * @var DefaultRuleCollection $objRuleCollection
         * @var array $tabValidatorConfig
         * @var StandardValidator $objValidator
         */

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Init validator
        $objValidator->setTabConfig(array_merge(
            $tabValidatorConfig,
            array('cache_require' => $boolCacheRequired)
        ));

        // Get data validation
        $tabErrorMessage = array();
        $tabErrorException = array();
        $boolIsValid = $objValidator->checkDataIsValid(
            $strName,
            $value,
            $tabRuleConfig,
            $tabErrorMessage,
            $tabErrorException
        );
        $tabErrorException = array_map(
            function($objException)
            {
                return (
                    ($objException instanceof Exception) ?
                        [
                            get_class($objException),
                            $objException->getMessage()
                        ]:
                        null
                );
            },
            $tabErrorException
        );

        // Get data validation, from cache, if required
        $tabCacheErrorMessage = array();
        $tabCacheErrorException = array();
        $boolCacheIsValid = false;
        if($boolCacheRequired)
        {
            // Init rule collection
            $objRuleCollection->removeRuleAll();

            // Get data validation, from cache
            $tabCacheErrorMessage = array();
            $tabCacheErrorException = array();
            $boolCacheIsValid = $objValidator->checkDataIsValid(
                $strName,
                $value,
                $tabRuleConfig,
                $tabCacheErrorMessage,
                $tabCacheErrorException
            );
            $tabCacheErrorException = array_map(
                function($objException)
                {
                    return (
                        ($objException instanceof Exception) ?
                            [
                                get_class($objException),
                                $objException->getMessage()
                            ]:
                            null
                    );
                },
                $tabCacheErrorException
            );
        }

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $boolExpectIsValid = $expectResult[0];
            $tabExpectErrorMessage = $expectResult[1];
            $tabExpectErrorException = $expectResult[2];

            // Set assertions (check data validation)
            $this->assertSame($boolExpectIsValid, $boolIsValid);
            $this->assertSame($tabExpectErrorMessage, $tabErrorMessage);
            $this->assertSame($tabExpectErrorException, $tabErrorException);

            // Set assertions (check validation cache)
            if($boolCacheRequired)
            {
                // Set assertions (check data validation, from cache)
                $this->assertSame($boolIsValid, $boolCacheIsValid);
                $this->assertSame($tabErrorMessage, $tabCacheErrorMessage);
                $this->assertSame($tabErrorException, $tabCacheErrorException);
            }
            else
            {
                // Set assertions (check cache not used)
                $this->assertSame(0, count($objCacheRepo->getTabSearchKey()));
            }

            // Print
            /*
            echo(sprintf('Check data ("%1$s") is valid:', $strName) . PHP_EOL);var_dump($boolIsValid);echo(PHP_EOL);
            echo(sprintf('Get data ("%1$s") error messages: ', $strName) . PHP_EOL);var_dump($tabErrorMessage);echo(PHP_EOL);
            echo(sprintf('Get data ("%1$s") error exceptions: ', $strName) . PHP_EOL);var_dump($tabErrorException);echo(PHP_EOL);
            echo(sprintf('Get data ("%1$s") validation cache key: ', $strName) . PHP_EOL);var_dump($objCacheRepo->getTabSearchKey());echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can check if data is valid.
     *
     * @return array
     */
    public function providerCheckDataIsValid()
    {
        // Return result
        return array(
            'Validator: check data is valid: fail validation (Invalid data name format)' => [
                7,
                'value',
                [
                    'type_string',
                    [
                        'string_start',
                        ['start_value' => 'Val']
                    ],
                    [
                        'string_end',
                        ['end_value' => ' 1']
                    ]
                ],
                true,
                DataNameInvalidFormatException::class
            ],
            'Validator: check data is valid: fail validation (Invalid rule configuration format 1)' => [
                'data-key-1',
                'value',
                [
                    7,
                    [
                        'string_start',
                        ['start_value' => 'Val']
                    ],
                    [
                        'string_end',
                        ['end_value' => ' 1']
                    ]
                ],
                false,
                RuleConfigInvalidFormatException::class
            ],
            'Validator: check data is valid: fail validation (Invalid rule configuration format 2)' => [
                'data-key-1',
                'value',
                [
                    'type_string',
                    [
                        true,
                        ['start_value' => 'Val']
                    ],
                    [
                        'string_end',
                        ['end_value' => ' 1']
                    ]
                ],
                true,
                RuleConfigInvalidFormatException::class
            ],
            'Validator: check data is valid: fail validation (Invalid rule configuration format 3)' => [
                'data-key-1',
                'value',
                [
                    'type_string',
                    [
                        'test' => 'string_start'
                    ],
                    [
                        'string_end',
                        ['end_value' => ' 1']
                    ]
                ],
                false,
                RuleConfigInvalidFormatException::class
            ],
            'Validator: check data is valid: fail validation (Invalid rule configuration format: rule not found)' => [
                'data-key-1',
                'value',
                [
                    'type_string',
                    [
                        'string_starter',
                        ['start_value' => 'Val']
                    ],
                    [
                        'string_end',
                        ['end_value' => ' 1']
                    ]
                ],
                true,
                RuleNotFoundException::class
            ],
            'Validator: check data is valid: fail validation (with cache)' => [
                'data-key-1',
                'value',
                [
                    'type_string',
                    [
                        'string_start',
                        ['start_value' => 'Val']
                    ],
                    [
                        'string_end',
                        ['end_value' => ' 1']
                    ]
                ],
                true,
                [
                    false,
                    [
                        'string_start' => 'data-key-1 doesn\'t start with Val.',
                        'string_end' => 'data-key-1 doesn\'t end with  1.'
                    ],
                    [
                        'string_start' => [
                            ValidationFailException::class,
                            'Validation failed! data-key-1 doesn\'t start with Val.'
                        ],
                        'string_end' => [
                            ValidationFailException::class,
                            'Validation failed! data-key-1 doesn\'t end with  1.'
                        ]
                    ]
                ]
            ],
            'Validator: check data is valid: fail validation (without cache)' => [
                'data-key-1',
                'value',
                [
                    'type_string',
                    [
                        'string_start',
                        ['start_value' => 'Val']
                    ],
                    [
                        'string_end',
                        ['end_value' => ' 1']
                    ]
                ],
                false,
                [
                    false,
                    [
                        'string_start' => 'data-key-1 doesn\'t start with Val.',
                        'string_end' => 'data-key-1 doesn\'t end with  1.'
                    ],
                    [
                        'string_start' => [
                            ValidationFailException::class,
                            'Validation failed! data-key-1 doesn\'t start with Val.'
                        ],
                        'string_end' => [
                            ValidationFailException::class,
                            'Validation failed! data-key-1 doesn\'t end with  1.'
                        ]
                    ]
                ]
            ],
            'Validator: check data is valid: success validation (with cache)' => [
                'data-key-1',
                'Value 1',
                [
                    'type_string',
                    [
                        'string_start',
                        ['start_value' => 'Val']
                    ],
                    [
                        'string_end',
                        ['end_value' => ' 1']
                    ]
                ],
                true,
                [true, [], []]
            ],
            'Validator: check data is valid: success validation (without cache)' => [
                'data-key-1',
                'Value 1',
                [
                    'type_string',
                    [
                        'string_start',
                        ['start_value' => 'Val']
                    ],
                    [
                        'string_end',
                        ['end_value' => ' 1']
                    ]
                ],
                false,
                [true, [], []]
            ],
            'Validator: check data is valid: success validation (without rule configuration; with cache)' => [
                'data-key-1',
                'Value 1',
                [],
                true,
                [true, [], []]
            ],
            'Validator: check data is valid: success validation (without rule configuration, cache)' => [
                'data-key-1',
                'Value 1',
                [],
                false,
                [true, [], []]
            ]
        );
    }



    /**
     * Test can check if data are valid.
     *
     * @param array $tabData
     * @param array $tabRuleConfig
     * @param boolean $boolCacheRequired
     * @param string|array $expectResult
     * @dataProvider providerCheckTabDataIsValid
     */
    public function testCanCheckTabDataIsValid(
        array $tabData,
        array $tabRuleConfig,
        $boolCacheRequired,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/validator/boot/ValidatorBootstrap.php');
        /**
         * @var DefaultRepository $objCacheRepo
         * @var DefaultRuleCollection $objRuleCollection
         * @var array $tabValidatorConfig
         * @var StandardValidator $objValidator
         */

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Init validator
        $objValidator->setTabConfig(array_merge(
            $tabValidatorConfig,
            array('cache_require' => $boolCacheRequired)
        ));

        // Get data validations
        $tabErrorMessage = array();
        $tabErrorException = array();
        $boolIsValid = $objValidator->checkTabDataIsValid(
            $tabData,
            $tabRuleConfig,
            $tabErrorMessage,
            $tabErrorException
        );
        $tabErrorException = array_map(
            function($tabErrorException)
            {
                return array_map(
                    function($objException)
                    {
                        return (
                            ($objException instanceof Exception) ?
                                [
                                    get_class($objException),
                                    $objException->getMessage()
                                ]:
                                null
                        );
                    },
                    $tabErrorException
                );
            },
            $tabErrorException
        );

        // Get data validations, from cache, if required
        $tabCacheErrorMessage = array();
        $tabCacheErrorException = array();
        $boolCacheIsValid = false;
        if($boolCacheRequired)
        {
            // Init rule collection
            $objRuleCollection->removeRuleAll();

            // Get data validations, from cache
            $tabCacheErrorMessage = array();
            $tabCacheErrorException = array();
            $boolCacheIsValid = $objValidator->checkTabDataIsValid(
                $tabData,
                $tabRuleConfig,
                $tabCacheErrorMessage,
                $tabCacheErrorException
            );
            $tabCacheErrorException = array_map(
                function($tabErrorException)
                {
                    return array_map(
                        function($objException)
                        {
                            return (
                                ($objException instanceof Exception) ?
                                    [
                                        get_class($objException),
                                        $objException->getMessage()
                                    ]:
                                    null
                            );
                        },
                        $tabErrorException
                    );
                },
                $tabCacheErrorException
            );
        }

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $boolExpectIsValid = $expectResult[0];
            $tabExpectErrorMessage = $expectResult[1];
            $tabExpectErrorException = $expectResult[2];

            // Set assertions (check data validations)
            $this->assertSame($boolExpectIsValid, $boolIsValid);
            $this->assertSame($tabExpectErrorMessage, $tabErrorMessage);
            $this->assertSame($tabExpectErrorException, $tabErrorException);

            // Set assertions (check validation cache)
            if($boolCacheRequired)
            {
                // Set assertions (check data validation, from cache)
                $this->assertSame($boolIsValid, $boolCacheIsValid);
                $this->assertSame($tabErrorMessage, $tabCacheErrorMessage);
                $this->assertSame($tabErrorException, $tabCacheErrorException);
            }
            else
            {
                // Set assertions (check cache not used)
                $this->assertSame(0, count($objCacheRepo->getTabSearchKey()));
            }

            // Print
            /*
            echo('get data:' . PHP_EOL);var_dump($tabData);echo(PHP_EOL);
            echo('Check data are valid:' . PHP_EOL);var_dump($boolIsValid);echo(PHP_EOL);
            echo('Get data error messages: ' . PHP_EOL);var_dump($tabErrorMessage);echo(PHP_EOL);
            echo('Get data error exceptions: ' . PHP_EOL);var_dump($tabErrorException);echo(PHP_EOL);
            echo('Get data validation cache key: ' . PHP_EOL);var_dump($objCacheRepo->getTabSearchKey());echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can check if data are valid.
     *
     * @return array
     */
    public function providerCheckTabDataIsValid()
    {
        // Return result
        return array(
            'Validator: check data are valid: fail validations (invalid data name format)' => [
                [
                    'value',
                    'data-key-2' => 2,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    'data-key-2' => [
                        [
                            'type_numeric',
                            ['integer_only_require' => 1]
                        ],
                        [
                            'compare_greater',
                            ['compare_value' => 7]
                        ]
                    ],
                    'data-key-3' => [
                        ['type_boolean']
                    ]
                ],
                true,
                DataNameInvalidFormatException::class
            ],
            'Validator: check data are valid: fail validations (Invalid rule configuration format: invalid data name format)' => [
                [
                    'data-key-1' => 'value',
                    'data-key-2' => 2,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    [
                        [
                            'type_numeric',
                            ['integer_only_require' => 1]
                        ],
                        [
                            'compare_greater',
                            ['compare_value' => 7]
                        ]
                    ],
                    'data-key-3' => [
                        ['type_boolean']
                    ]
                ],
                false,
                DataNameInvalidFormatException::class
            ],
            'Validator: check data are valid: fail validations (Invalid rule configuration format 1)' => [
                [
                    'data-key-1' => 'value',
                    'data-key-2' => 2,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    'data-key-2' => false,
                    'data-key-3' => [
                        ['type_boolean']
                    ]
                ],
                true,
                RuleConfigInvalidFormatException::class
            ],
            'Validator: check data are valid: fail validations (Invalid rule configuration format 2)' => [
                [
                    'data-key-1' => 'value',
                    'data-key-2' => 2,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    'data-key-2' => [
                        null,
                        [
                            'compare_greater',
                            ['compare_value' => 7]
                        ]
                    ],
                    'data-key-3' => [
                        ['type_boolean']
                    ]
                ],
                false,
                RuleConfigInvalidFormatException::class
            ],
            'Validator: check data are valid: fail validations (Invalid rule configuration format 3)' => [
                [
                    'data-key-1' => 'value',
                    'data-key-2' => 2,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    'data-key-2' => [
                        [
                            'test-1' => 'type_numeric',
                            'test-2' => ['integer_only_require' => 1]
                        ],
                        [
                            'compare_greater',
                            ['compare_value' => 7]
                        ]
                    ],
                    'data-key-3' => [
                        ['type_boolean']
                    ]
                ],
                true,
                RuleConfigInvalidFormatException::class
            ],
            'Validator: check data are valid: fail validations (Invalid rule configuration format: rule not found)' => [
                [
                    'data-key-1' => 'value',
                    'data-key-2' => 2,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    'data-key-2' => [
                        [
                            'type_num',
                            ['integer_only_require' => 1]
                        ],
                        [
                            'compare_greater',
                            ['compare_value' => 7]
                        ]
                    ],
                    'data-key-3' => [
                        ['type_boolean']
                    ]
                ],
                false,
                RuleNotFoundException::class
            ],
            'Validator: check data are valid: fail validations (with cache)' => [
                [
                    'data-key-1' => 'value',
                    'data-key-2' => 2,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    'data-key-2' => [
                        [
                            'type_numeric',
                            ['integer_only_require' => 1]
                        ],
                        [
                            'compare_greater',
                            ['compare_value' => 7]
                        ]
                    ],
                    'data-key-3' => [
                        ['type_boolean']
                    ],
                    'data-key-4' => []
                ],
                true,
                [
                    false,
                    [
                        'data-key-1' => [
                            'string_start' => 'data-key-1 doesn\'t start with Val.',
                            'string_end' => 'data-key-1 doesn\'t end with  1.'
                        ],
                        'data-key-2' => [
                            'compare_greater' => 'data-key-2 must be greater than 7.'
                        ]
                    ],
                    [
                        'data-key-1' => [
                            'string_start' => [
                                ValidationFailException::class,
                                'Validation failed! data-key-1 doesn\'t start with Val.'
                            ],
                            'string_end' => [
                                ValidationFailException::class,
                                'Validation failed! data-key-1 doesn\'t end with  1.'
                            ]
                        ],
                        'data-key-2' => [
                            'compare_greater' => [
                                ValidationFailException::class,
                                'Validation failed! data-key-2 must be greater than 7.'
                            ]
                        ]
                    ]
                ]
            ],
            'Validator: check data are valid: fail validations (without cache)' => [
                [
                    'data-key-1' => 'value',
                    'data-key-2' => 2,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    'data-key-2' => [
                        [
                            'type_numeric',
                            ['integer_only_require' => 1]
                        ],
                        [
                            'compare_greater',
                            ['compare_value' => 7]
                        ]
                    ],
                    'data-key-3' => [
                        ['type_boolean']
                    ]
                ],
                false,
                [
                    false,
                    [
                        'data-key-1' => [
                            'string_start' => 'data-key-1 doesn\'t start with Val.',
                            'string_end' => 'data-key-1 doesn\'t end with  1.'
                        ],
                        'data-key-2' => [
                            'compare_greater' => 'data-key-2 must be greater than 7.'
                        ]
                    ],
                    [
                        'data-key-1' => [
                            'string_start' => [
                                ValidationFailException::class,
                                'Validation failed! data-key-1 doesn\'t start with Val.'
                            ],
                            'string_end' => [
                                ValidationFailException::class,
                                'Validation failed! data-key-1 doesn\'t end with  1.'
                            ]
                        ],
                        'data-key-2' => [
                            'compare_greater' => [
                                ValidationFailException::class,
                                'Validation failed! data-key-2 must be greater than 7.'
                            ]
                        ]
                    ]
                ]
            ],
            'Validator: check data are valid: success validations (with cache)' => [
                [
                    'data-key-1' => 'Value 1',
                    'data-key-2' => 9,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    'data-key-2' => [
                        [
                            'type_numeric',
                            ['integer_only_require' => 1]
                        ],
                        [
                            'compare_greater',
                            ['compare_value' => 7]
                        ]
                    ],
                    'data-key-3' => [
                        ['type_boolean']
                    ]
                ],
                true,
                [true, [], []]
            ],
            'Validator: check data are valid: success validations (without cache)' => [
                [
                    'data-key-1' => 'Value 1',
                    'data-key-2' => 9,
                    'data-key-3' => true,
                    'data-key-4' => null
                ],
                [
                    'data-key-1' => [
                        'type_string',
                        [
                            'string_start',
                            ['start_value' => 'Val']
                        ],
                        [
                            'string_end',
                            ['end_value' => ' 1']
                        ]
                    ],
                    'data-key-2' => [
                        [
                            'type_numeric',
                            ['integer_only_require' => 1]
                        ],
                        [
                            'compare_greater',
                            ['compare_value' => 7]
                        ]
                    ],
                    'data-key-3' => [
                        ['type_boolean']
                    ],
                    'data-key-4' => []
                ],
                false,
                [true, [], []]
            ]
        );
    }



}