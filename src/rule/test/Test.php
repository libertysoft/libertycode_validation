<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/rule/test/RuleCollectionTest.php');



// Test add rule
echo('Test add : <br />');

echo('<pre>');print_r($objRuleCollection->beanGetTabData());echo('</pre>');

echo('<br /><br /><br />');



// Test data validation
echo('Test validation : <br />');

$tabInfo = array(
    // Info 1 Ko (bad rule key format)
    [
        7,
        'name-1',
        'Value 1',
        ['not_empty_require' => 1]
    ],
    // Info 2 Ko (validation fails)
    [
        'string',
        'name-1',
        7,
        ['not_empty_require' => 1]
    ],
    // Info 3 Ok
    [
        'string',
        'name-1',
        'Value 1',
        ['not_empty_require' => 1]
    ],
    // Info 4 Ko (validation fails)
    [
        'string',
        'name-1',
        '',
        ['not_empty_require' => 1]
    ],
    // Info 5 Ko (bad data name format)
    [
        'integer',
        null,
        'test',
        null
    ],
    // Info 6 Ko (validation fails)
    [
        'integer',
        'name-2',
        'test',
        null
    ],
    // Info 7 Ok
    [
        'integer',
        'name-2',
        7,
        null
    ],
    // Info 8 Ko (validation fails)
    [
        'BetweenRuleTest',
        'name-3',
        10,
        ['start' => 4, 'end' => 7]
    ],
    // Info 9 Ok
    [
        'BetweenRuleTest',
        'name-3',
        5,
        ['start' => 4, 'end' => 7]
    ]
);

foreach($tabInfo as $info)
{
    echo('Test validation info: <br />');

    try{
        // Get info
        $strKey = $info[0];
        $strName = $info[1];
        $value = $info[2];
        $tabConfig = $info[3];

        // Get rule
        $boolExists = $objRuleCollection->checkExists($strKey);
        $objRule = $objRuleCollection->getObjRule($strKey);

        echo('Info: <pre>');
        print_r(array(
            $strKey,
            $strName,
            (is_object($value) ? get_class($value) : $value),
            $tabConfig
        ));
        echo('</pre>');
        echo('Rule exits: <pre>');var_dump($boolExists);echo('</pre>');

        // Test validation, if required
        if($boolExists)
        {
            // Get validation
            $boolIsValid = $objRule->checkIsValid($strName, $value, $tabConfig);
            $strErrorMessage = $objRule->getStrErrorMessage($strName, $value, $tabConfig);
            $objErrorException = $objRule->getObjErrorException($strName, $value, $tabConfig);

            echo('Rule key: <pre>');print_r($objRule->getStrKey());echo('</pre>');
            echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
            echo('Get error message: <pre>');var_dump($strErrorMessage);echo('</pre>');
            echo('Get error exception: <pre>');
            var_dump(
                (!is_null($objErrorException)) ?
                    get_class($objErrorException) . ': ' . $objErrorException->getMessage() :
                    $objErrorException
            );
            echo('</pre>');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test remove rule
echo('Test remove: <br />');

echo('Before removing: <pre>');print_r($objRuleCollection->getTabKey());echo('</pre>');

$objRuleCollection->removeRuleAll();
echo('After removing: <pre>');print_r($objRuleCollection->getTabKey());echo('</pre>');

echo('<br /><br /><br />');


