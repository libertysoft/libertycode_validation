<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\library;



class ConstValidator
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CACHE_REPO = 'objCacheRepo';
	
	

    // Configuration
    const TAB_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_CONFIG_KEY_CACHE_KEY_PATTERN = 'cache_key_pattern';
    const TAB_CONFIG_KEY_CACHE_VALUE_IS_VALID_KEY = 'cache_value_is_valid_key';
    const TAB_CONFIG_KEY_CACHE_VALUE_ERROR_MESSAGE_KEY = 'cache_value_error_message_key';
    const TAB_CONFIG_KEY_CACHE_VALUE_ERROR_EXCEPTION_KEY = 'cache_value_error_exception_key';
    const TAB_CONFIG_KEY_CACHE_SET_CONFIG = 'cache_set_config';

    // Cache configuration
    const CACHE_DEFAULT_KEY_PATTERN = '%1$s%2$s%3$s';
    const CACHE_DEFAULT_VALUE_IS_VALID_KEY = 'is_valid';
    const CACHE_DEFAULT_VALUE_ERROR_MESSAGE_KEY = 'error_message';
    const CACHE_DEFAULT_VALUE_ERROR_EXCEPTION_KEY = 'error_exception';

    // Rule configuration
    const TAB_RULE_CONFIG_KEY_RULE_NAME = 'rule_name';
    const TAB_RULE_CONFIG_KEY_RULE_CONFIG = 'rule_config';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default validator configuration standard.';
    const EXCEPT_MSG_CACHE_REPO_INVALID_FORMAT = 'Following cache repository "%1$s" invalid! It must be a cache repository object.';
    const EXCEPT_MSG_DATA_NAME_INVALID_FORMAT = 'Following data name "%1$s" invalid! The name must be a string not empty.';
    const EXCEPT_MSG_RULE_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the rule configuration standard.';



}