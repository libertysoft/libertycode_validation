<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\standard\library;



class ConstStandardValidation
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_VALIDATOR = 'objValidator';



    // Configuration
    const TAB_CONFIG_KEY_DATA = 'data';
    const TAB_CONFIG_KEY_RULE_CONFIG = 'rule_config';



    // Exception message constants
    const EXCEPT_MSG_VALIDATOR_INVALID_FORMAT = 'Following validator "%1$s" invalid! It must be a validator object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the standard validation configuration standard.';



}