<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\library;



class ConstValidation
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default validation configuration standard.';
    const EXCEPT_MSG_DATA_NAME_INVALID_FORMAT = 'Following data name "%1$s" invalid! The name must be a string not empty.';



}