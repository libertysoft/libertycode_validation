<?php
/**
 * Description :
 * This class allows to define sub-rule class.
 * Sub-rule allows to validate data,
 * from specified rule configuration.
 *
 * Sub-rule uses the following specified configuration:
 * [
 *     Validator rule configuration,
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name,
 *         '%2$s' replaced by data value,
 *         '%3$s' replaced by comma separated list of sub-rule keys."
 * ]
 *
 * Sub-rule uses the following specified validation configuration:
 * [
 *     Validator rule validation configuration,
 *
 *     rule_config(required):
 *         @see ValidatorInterface::checkDataIsValid() rule configuration array format
 * ]
 *
 * Sub-rule uses the following specified error configuration:
 * [
 *     Validator rule error configuration,
 *
 *     rule_config(required): [
 *         @see ValidatorInterface::checkDataIsValid() rule configuration array format,
 *
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name,
 *         '%2$s' replaced by data value,
 *         '%3$s' replaced by comma separated list of sub-rule names."
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\sub_rule\model;

use liberty_code\validation\validator\rule\model\ValidatorRule;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\validator\library\ConstValidator;
use liberty_code\validation\validator\library\ToolBoxRuleConfig;
use liberty_code\validation\validator\rule\sub_rule\library\ConstSubRule;
use liberty_code\validation\validator\rule\sub_rule\exception\ValidConfigInvalidFormatException;
use liberty_code\validation\validator\rule\sub_rule\exception\ErrorConfigInvalidFormatException;



abstract class SubRule extends ValidatorRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified data is valid,
     * from specified rule configuration.
     *
     * Rule configuration format:
     * @see SubRule validation configuration rule_config array format.
     *
     * @param string $strName
     * @param mixed $value
     * @param array $tabRuleConfig
     * @param array $tabConfig
     * @return boolean
     */
    abstract protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabRuleConfig,
        array $tabConfig
    );



    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $tabRuleConfig = $tabConfig[ConstSubRule::TAB_VALID_CONFIG_KEY_RULE_CONFIG];

            // Check valid data
            $result = $this->checkIsValidEngine(
                $strName,
                $value,
                $tabRuleConfig,
                $tabConfig
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = null;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strValue = (
                ToolBoxString::checkConvertString($value) ?
                    strval($value) :
                    ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
            );
            $tabRuleConfig = ToolBoxRuleConfig::getTabFormatRuleConfig(
                $tabConfig[ConstSubRule::TAB_ERROR_CONFIG_KEY_RULE_CONFIG]
            );
            $tabRuleNm = array_map(
                function ($ruleConfig) {return $ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_NAME];},
                $tabRuleConfig
            );
            $strRuleNm = implode(', ', $tabRuleNm);
            $result = sprintf($strErrorMsgPattern, $strName, $strValue, $strRuleNm);
        }

        // Return result
        return $result;
    }



}