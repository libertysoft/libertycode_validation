<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\string_contain\exception;

use Exception;

use liberty_code\validation\rule\standard\string_contain\library\ConstStringContainRule;



class ValidConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStringContainRule::EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid contain value
            array_key_exists(ConstStringContainRule::TAB_VALID_CONFIG_KEY_CONTAIN_VALUE, $config) &&
            is_string($config[ConstStringContainRule::TAB_VALID_CONFIG_KEY_CONTAIN_VALUE]) &&
            ($config[ConstStringContainRule::TAB_VALID_CONFIG_KEY_CONTAIN_VALUE] != '') &&

            // Check valid encoding name
            (
                (!isset($config[ConstStringContainRule::TAB_VALID_CONFIG_KEY_ENCODING_NAME])) ||
                (
                    is_string($config[ConstStringContainRule::TAB_VALID_CONFIG_KEY_ENCODING_NAME]) &&
                    (trim($config[ConstStringContainRule::TAB_VALID_CONFIG_KEY_ENCODING_NAME]) != '')
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}