<?php
/**
 * Description :
 * This class allows to describe behavior of validation class.
 * Validation contains all information to validate specific data,
 * from specific rule configuration.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\api;

use liberty_code\validation\validator\api\ValidatorInterface;



interface ValidationInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

    /**
     * Check if data are valid,
     * from configuration.
     * If specified data provided, return specified data validation.
     * Else, return all data validations.
     *
     * @param string $strName = null
     * @return boolean
     */
    public function checkDataIsValid($strName = null);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();




    /**
     * Get data array.
     *
     * Return array format:
     * @see ValidatorInterface::checkTabDataIsValid() data array format.
     *
     * @return array
     */
    public function getTabData();



    /**
     * Get rule configurations array.
     *
     * Return array format:
     * @see ValidatorInterface::checkTabDataIsValid() rule configurations array format.
     *
     * @return array
     */
    public function getTabRuleConfig();



    /**
     * Get error messages array,
     * from configuration.
     *
     * Return array format:
     * - If specified data provided:
     *     Return specified data failed rules names:
     *     @see ValidatorInterface::checkDataIsValid() error messages array format.
     * - Else:
     *     Return all failed data and failed rules names:
     *     @see ValidatorInterface::checkTabDataIsValid() error messages array format.
     *
     * @param string $strName = null
     * @return array
     */
    public function getTabErrorMessage($strName = null);



    /**
     * Get error exceptions array,
     * from configuration.
     *
     * Return array format:
     * - If specified data provided:
     *     Return specified data failed rules names:
     *     @see ValidatorInterface::checkDataIsValid() error exceptions array format.
     * - Else:
     *     Return all failed data and failed rules names:
     *     @see ValidatorInterface::checkTabDataIsValid() error exceptions array format.
     *
     * @param string $strName = null
     * @return array
     */
    public function getTabErrorException($strName = null);



	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}