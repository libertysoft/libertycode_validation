<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\test\feature\attribute\specification\type;

use PHPUnit\Framework\TestCase;

use Exception;
use liberty_code\validation\rule\exception\ValidationFailException;
use liberty_code\validation\validator\standard\model\StandardValidator;
use liberty_code\validation\validation\exception\DataNameInvalidFormatException;
use liberty_code\validation\validation\api\ValidationInterface;
use liberty_code\validation\validation\model\DefaultValidation;
use liberty_code\validation\validation\standard\exception\ConfigInvalidFormatException;
use liberty_code\validation\validation\standard\model\StandardValidation;



/**
 * @cover ValidationInterface
 * @cover DefaultValidation
 * @cover StandardValidation
 */
class ValidationTest extends TestCase
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods test
    // ******************************************************************************

    /**
     * Test can use standard validation object.
     *
     * @param null|array $tabConfig
     * @param null|mixed $name
     * @param string|array $expectResult
     * @dataProvider providerUseStandardValidation
     */
    public function testCanUseStandardValidation(
        $tabConfig,
        $name,
        $expectResult
    )
    {
        // Load
        $strRootAppPath = dirname(__FILE__) . '/../..';
        require($strRootAppPath . '/validator/boot/ValidatorBootstrap.php');
        /**
         * @var StandardValidator $objValidator
         */

        // Init var
        $boolExceptionExpected = is_string($expectResult);

        // Expect exception, if required
        if($boolExceptionExpected)
        {
            $this->expectException($expectResult);
        }

        // Init validation
        $objValidation = new StandardValidation($objValidator, $tabConfig);

        // Get validation details
        $tabData = $objValidation->getTabData();
        $tabRuleConfig = $objValidation->getTabRuleConfig();

        // Get data validation
        $boolIsValid = $objValidation->checkDataIsValid($name);
        $tabErrorMessage = $objValidation->getTabErrorMessage($name);
        $tabErrorException = $objValidation->getTabErrorException($name);
        $tabErrorException = (
            (!is_null($name)) ?
                array_map(
                    function($objException)
                    {
                        return (
                            ($objException instanceof Exception) ?
                                [
                                    get_class($objException),
                                    $objException->getMessage()
                                ]:
                            null
                        );
                    },
                    $tabErrorException
                ) :
                array_map(
                    function($tabErrorException)
                    {
                        return array_map(
                            function($objException)
                            {
                                return (
                                    ($objException instanceof Exception) ?
                                        [
                                            get_class($objException),
                                            $objException->getMessage()
                                        ]:
                                        null
                                );
                            },
                            $tabErrorException
                        );
                    },
                    $tabErrorException
                )
        );

        // Set assertions, if required
        if(!$boolExceptionExpected)
        {
            // Get info
            $tabExpectData = $expectResult[0];
            $tabExpectRuleConfig = $expectResult[1];
            $boolExpectIsValid = $expectResult[2];
            $tabExpectErrorMessage = $expectResult[3];
            $tabExpectErrorException = $expectResult[4];

            // Set assertions (check validation details)
            $this->assertSame($objValidation->getTabConfig(), $tabConfig);
            $this->assertSame($tabExpectData, $tabData);
            $this->assertSame($tabExpectRuleConfig, $tabRuleConfig);

            // Set assertions (check data validation)
            $this->assertSame($boolExpectIsValid, $boolIsValid);
            $this->assertSame($tabExpectErrorMessage, $tabErrorMessage);
            $this->assertSame($tabExpectErrorException, $tabErrorException);

            // Set assertions (check validation settings)
            $objValidation->setData($tabData);
            $objValidation->setRuleConfig($tabRuleConfig);
            $this->assertSame($tabConfig, $objValidation->getTabConfig());

            // Print
            /*
            echo('Get validation data: ' . PHP_EOL);var_dump($tabData);echo(PHP_EOL);
            echo('Get validation rule configurations: ' . PHP_EOL);var_dump($tabRuleConfig);echo(PHP_EOL);
            echo('Check validation is valid: ' . PHP_EOL);var_dump($boolIsValid);echo(PHP_EOL);
            echo('Get validation error messages: ' . PHP_EOL);var_dump($tabErrorMessage);echo(PHP_EOL);
            echo('Get validation error exceptions: ' . PHP_EOL);var_dump($tabErrorException);echo(PHP_EOL);
            //*/
        }
    }



    /**
     * Data provider,
     * to test can use standard validation object.
     *
     * @return array
     */
    public function providerUseStandardValidation()
    {
        // Init var
        $tabSuccessData = array(
            'data-key-1' => 'Value 1',
            'data-key-2' => 7,
            'data-key-3' => true,
            'data-key-4' => null
        );
        $tabFailData = array(
            'data-key-1' => 'test',
            'data-key-2' => -7,
            'data-key-3' => true,
            'data-key-4' => null
        );
        $tabRuleConfig = array(
            'data-key-1' => [
                'type_string',
                [
                    'string_start',
                    ['start_value' => 'Val']
                ],
                [
                    'string_end',
                    ['end_value' => ' 1']
                ]
            ],
            'data-key-2' => [
                [
                    'type_numeric',
                    ['integer_only_require' => 1]
                ],
                [
                    'compare_greater',
                    ['compare_value' => 0]
                ]
            ],
            'data-key-3' => ['type_boolean']
        );

        // Return result
        return array(
            'Use standard validation: fail to use validation (invalid data format 1)' => [
                [
                    'data' => 'test',
                    'rule_config' => $tabRuleConfig
                ],
                'data-key-1',
                ConfigInvalidFormatException::class
            ],
            'Use standard validation: fail to use validation (invalid data format 2)' => [
                [
                    'data' => [
                        'Value 1',
                        'data-key-2' => 7,
                        true,
                        'data-key-4' => null
                    ],
                    'rule_config' => $tabRuleConfig
                ],
                'data-key-1',
                ConfigInvalidFormatException::class
            ],
            'Use standard validation: fail to use validation (Invalid rule configuration format 1)' => [
                [
                    'data' => $tabSuccessData,
                    'rule_config' => 'test'
                ],
                'data-key-1',
                ConfigInvalidFormatException::class
            ],
            'Use standard validation: fail to use validation (Invalid rule configuration format 2)' => [
                [
                    'data' => $tabSuccessData,
                    'rule_config' => [
                        'data-key-1' => [
                            'type_string',
                            [
                                'string_start',
                                ['start_value' => 'Val']
                            ],
                            [
                                'string_end',
                                ['end_value' => ' 1']
                            ]
                        ],
                        [
                            [
                                'type_numeric',
                                ['integer_only_require' => 1]
                            ],
                            [
                                'compare_greater',
                                ['compare_value' => 0]
                            ]
                        ],
                        'data-key-3' => ['type_boolean']
                    ]
                ],
                'data-key-1',
                ConfigInvalidFormatException::class
            ],
            'Use standard validation: fail to use validation (Invalid rule configuration format 3)' => [
                [
                    'data' => $tabSuccessData,
                    'rule_config' => [
                        'data-key-1' => [
                            'type_string',
                            [
                                'string_start',
                                ['start_value' => 'Val']
                            ],
                            [
                                'string_end',
                                ['end_value' => ' 1']
                            ]
                        ],
                        [
                            null,
                            [
                                'compare_greater',
                                ['compare_value' => 0]
                            ]
                        ],
                        'data-key-3' => ['type_boolean']
                    ]
                ],
                'data-key-1',
                ConfigInvalidFormatException::class
            ],
            'Use standard validation: fail to use validation (invalid data name format 1)' => [
                [
                    'data' => $tabSuccessData,
                    'rule_config' => $tabRuleConfig
                ],
                7,
                DataNameInvalidFormatException::class
            ],
            'Use standard validation: fail to use validation (invalid data name format 2)' => [
                [
                    'data' => $tabSuccessData,
                    'rule_config' => $tabRuleConfig
                ],
                'test',
                DataNameInvalidFormatException::class
            ],
            'Use standard validation: success to use validation: fail data validation' => [
                [
                    'data' => $tabFailData,
                    'rule_config' => $tabRuleConfig
                ],
                'data-key-1',
                [
                    $tabFailData,
                    $tabRuleConfig,
                    false,
                    [
                        'string_start' => 'data-key-1 doesn\'t start with Val.',
                        'string_end' => 'data-key-1 doesn\'t end with  1.'
                    ],
                    [
                        'string_start' => [
                            ValidationFailException::class,
                            'Validation failed! data-key-1 doesn\'t start with Val.'
                        ],
                        'string_end' => [
                            ValidationFailException::class,
                            'Validation failed! data-key-1 doesn\'t end with  1.'
                        ]
                    ]
                ]
            ],
            'Use standard validation: success to use validation: success data validation (fail data validations)' => [
                [
                    'data' => $tabFailData,
                    'rule_config' => $tabRuleConfig
                ],
                'data-key-3',
                [
                    $tabFailData,
                    $tabRuleConfig,
                    true,
                    [],
                    []
                ]
            ],
            'Use standard validation: success to use validation: success data validation (success data validations)' => [
                [
                    'data' => $tabSuccessData,
                    'rule_config' => $tabRuleConfig
                ],
                'data-key-1',
                [
                    $tabSuccessData,
                    $tabRuleConfig,
                    true,
                    [],
                    []
                ]
            ],
            'Use standard validation: success to use validation: success data validation (fail data validations; without rule configurations)' => [
                [
                    'data' => $tabFailData,
                    'rule_config' => $tabRuleConfig
                ],
                'data-key-4',
                [
                    $tabFailData,
                    $tabRuleConfig,
                    true,
                    [],
                    []
                ]
            ],
            'Use standard validation: success to use validation: success data validation (success data validations; without rule configurations)' => [
                [
                    'data' => $tabSuccessData,
                    'rule_config' => $tabRuleConfig
                ],
                'data-key-4',
                [
                    $tabSuccessData,
                    $tabRuleConfig,
                    true,
                    [],
                    []
                ]
            ],
            'Use standard validation: success to use validation: fail data validations' => [
                [
                    'data' => $tabFailData,
                    'rule_config' => $tabRuleConfig
                ],
                null,
                [
                    $tabFailData,
                    $tabRuleConfig,
                    false,
                    [
                        'data-key-1' => [
                            'string_start' => 'data-key-1 doesn\'t start with Val.',
                            'string_end' => 'data-key-1 doesn\'t end with  1.'
                        ],
                        'data-key-2' => [
                            'compare_greater' => 'data-key-2 must be greater than 0.'
                        ]
                    ],
                    [
                        'data-key-1' => [
                            'string_start' => [
                                ValidationFailException::class,
                                'Validation failed! data-key-1 doesn\'t start with Val.'
                            ],
                            'string_end' => [
                                ValidationFailException::class,
                                'Validation failed! data-key-1 doesn\'t end with  1.'
                            ]
                        ],
                        'data-key-2' => [
                            'compare_greater' => [
                                ValidationFailException::class,
                                'Validation failed! data-key-2 must be greater than 0.'
                            ]
                        ]
                    ]
                ]
            ],
            'Use standard validation: success to use validation: success data validations' => [
                [
                    'data' => $tabSuccessData,
                    'rule_config' => $tabRuleConfig
                ],
                null,
                [
                    $tabSuccessData,
                    $tabRuleConfig,
                    true,
                    [],
                    []
                ]
            ],
            'Use standard validation: success to use validation: success data validations (without data)' => [
                [
                    'data' => [],
                    'rule_config' => $tabRuleConfig
                ],
                null,
                [
                    [],
                    $tabRuleConfig,
                    true,
                    [],
                    []
                ]
            ],
            'Use standard validation: success to use validation: success data validations (without rule configurations)' => [
                [
                    'data' => $tabSuccessData,
                    'rule_config' => []
                ],
                null,
                [
                    $tabSuccessData,
                    [],
                    true,
                    [],
                    []
                ]
            ]
        );
    }



}