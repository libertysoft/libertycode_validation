<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/rule/standard/test/RuleCollectionTest.php');



// Test data validation
echo('Test validation : <br />');

$objDateBefore = new DateTime();
$objDateBefore->sub(new DateInterval('P1D'));
$objDate = new DateTime();
$objDateAfter = new DateTime();
$objDateAfter->add(new DateInterval('P1D'));
$tabInfo = array(
    // Test equal Ko
    [
        'compare_equal',
        'key_1',
        'Value 1',
        null
    ],

    // Test equal Ko
    [
        'compare_equal',
        'key_1',
        'value 1',
        ['compare_value' => 'Value 1']
    ],

    // Test equal Ko
    [
        'compare_equal',
        'key_1',
        'value 1',
        ['compare_value' => $objDate]
    ],

    // Test equal Ok
    [
        'compare_equal',
        'key_1',
        'Value 1',
        ['compare_value' => 'Value 1']
    ],

    // Test equal Ko
    [
        'compare_equal',
        'key_1',
        $objDate,
        ['compare_value' => 'Value 1']
    ],

    // Test equal Ko
    [
        'compare_equal',
        'key_1',
        $objDate,
        ['compare_value' => $objDateAfter]
    ],

    // Test equal Ok
    [
        'compare_equal',
        'key_1',
        $objDate,
        ['compare_value' => $objDate]
    ],

    // Test in Ko
    [
        'compare_in',
        'key_2',
        '7',
        null
    ],

    // Test in Ko
    [
        'compare_in',
        'key_2',
        7,
        ['compare_value' => ['6', '7', '8']]
    ],

    // Test in Ok
    [
        'compare_in',
        'key_2',
        '7',
        ['compare_value' => ['6', '7', '8']]
    ],

    // Test less Ko
    [
        'compare_less',
        'key_1',
        true,
        [
            'compare_value' => 7,
            'equal_enable_require' => true
        ]
    ],

    // Test less Ok
    [
        'compare_less',
        'key_1',
        4,
        [
            'compare_value' => 7,
            'equal_enable_require' => true
        ]
    ],

    // Test less Ok
    [
        'compare_less',
        'key_1',
        2,
        [
            'compare_value' => 3,
            'equal_enable_require' => false
        ]
    ],

    // Test greater Ko
    [
        'compare_greater',
        'key_2',
        $objDate,
        ['compare_value' => $objDateAfter]
    ],

    // Test greater Ok
    [
        'compare_greater',
        'key_2',
        $objDate,
        ['compare_value' => $objDateBefore]
    ],

    // Test between Ko
    [
        'compare_between',
        'key_1',
        7,
        [
            'greater_compare_value' => 3,
            'less_compare_value' => 7,
            'less_equal_enable_require' => false
        ]
    ],

    // Test between Ok
    [
        'compare_between',
        'key_1',
        3,
        [
            'greater_compare_value' => 3,
            'less_compare_value' => 7,
            'less_equal_enable_require' => false
        ]
    ],

    // Test between Ok
    [
        'compare_between',
        'key_1',
        5,
        [
            'greater_compare_value' => 3,
            'less_compare_value' => 7,
            'less_equal_enable_require' => false
        ]
    ],

    // Test between Ko
    [
        'compare_between',
        'key_2',
        $objDateBefore,
        [
            'greater_compare_value' => $objDateBefore,
            'greater_equal_enable_require' => false,
            'less_compare_value' => $objDateAfter
        ]
    ],

    // Test between Ok
    [
        'compare_between',
        'key_2',
        $objDateAfter,
        [
            'greater_compare_value' => $objDateBefore,
            'greater_equal_enable_require' => false,
            'less_compare_value' => $objDateAfter
        ]
    ],

    // Test between Ok
    [
        'compare_between',
        'key_2',
        $objDate,
        [
            'greater_compare_value' => $objDateBefore,
            'greater_equal_enable_require' => false,
            'less_compare_value' => $objDateAfter
        ]
    ]
);

foreach($tabInfo as $info)
{
    echo('Test validation info: <br />');

    try{
        // Get info
        $strKey = $info[0];
        $strName = $info[1];
        $value = $info[2];
        $tabConfig = $info[3];

        // Get rule
        $boolExists = $objRuleCollection->checkExists($strKey);
        $objRule = $objRuleCollection->getObjRule($strKey);

        echo('Info: <pre>');
        print_r(array(
            $strKey,
            $strName,
            (is_object($value) ? get_class($value) : $value),
            $tabConfig
        ));
        echo('</pre>');

        // Test validation, if required
        if($boolExists)
        {
            // Get validation
            $boolIsValid = $objRule->checkIsValid($strName, $value, $tabConfig);
            $strErrorMessage = $objRule->getStrErrorMessage($strName, $value, $tabConfig);
            $objErrorException = $objRule->getObjErrorException($strName, $value, $tabConfig);

            echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
            echo('Get error message: <pre>');var_dump($strErrorMessage);echo('</pre>');
            echo('Get error exception: <pre>');
            var_dump(
                (!is_null($objErrorException)) ?
                    get_class($objErrorException) . ': ' . $objErrorException->getMessage() :
                    $objErrorException
            );
            echo('</pre>');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


