<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\compare_greater\library;



class ConstCompareGreaterRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'compare_greater';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s must be greater than %3$s.';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_COMPARE_VALUE = 'compare_value';
    const TAB_VALID_CONFIG_KEY_EQUAL_ENABLE_REQUIRE = 'equal_enable_require';

    // Error configuration
    const TAB_ERROR_CONFIG_KEY_COMPARE_VALUE = 'compare_value';


	
    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the compare greater rule validation configuration standard.';
    const EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the compare greater rule error configuration standard.';



}