<?php
/**
 * Description :
 * This class allows to define type object rule class.
 * Type object rule allows to check if specified data is object.
 *
 * Type object rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Type object rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     // Scope of potential data value class paths
 *     class_path(optional): [
 *         "string class path 1",
 *         ...,
 *         "string class path N"
 *     ],
 *
 *     // Data value class can be a subclass of class scoped above
 *     extend_enable_require(optional: got true if not found): true / false,
 * ]
 *
 * Type object rule uses the following specified error configuration:
 * [
 *     Default rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\type_object\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\type_object\library\ConstTypeObjectRule;
use liberty_code\validation\rule\standard\type_object\exception\ValidConfigInvalidFormatException;



class TypeObjectRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check extend enable required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see TypeObjectRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkExtendEnableRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            (!is_array($tabValidConfig)) ||
            (!array_key_exists(ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_EXTEND_ENABLE_REQUIRE, $tabValidConfig)) ||
            (intval($tabValidConfig[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_EXTEND_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabClassPath = $this->getTabClassPath($tabConfig);
        $boolExtendEnable = $this->checkExtendEnableRequired($tabConfig);
        $result = is_object($value);

        // Check class path, if required
        if($result && (count($tabClassPath) > 0))
        {
            // Run each class path
            $result = false;
            for($intCpt = 0; ($intCpt < count($tabClassPath)) && (!$result); $intCpt++)
            {
                $strClassPath = $tabClassPath[$intCpt];
                $result = (
                    // Check valid class path
                    (get_class($value) == $strClassPath) ||

                    // Check valid sub class path, if required
                    ($boolExtendEnable && is_subclass_of(get_class($value), $strClassPath))
                );
            }
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstTypeObjectRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstTypeObjectRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * Get index array of class paths.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see TypeObjectRule
     *
     * @param array $tabConfig = null
     * @return array
     */
    protected function getTabClassPath(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            isset($tabValidConfig[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_CLASS_PATH]) ?
                $tabValidConfig[ConstTypeObjectRule::TAB_VALID_CONFIG_KEY_CLASS_PATH] :
                array()
        );

        // Return result
        return $result;
    }



}