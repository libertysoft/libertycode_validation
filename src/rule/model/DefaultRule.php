<?php
/**
 * Description :
 * This class allows to define default rule class.
 * Can be consider is base of all rule type.
 *
 * Default rule uses the following specified configuration:
 * [
 *     key(optional: got static class name if not found): "string rule key",
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value"
 * ]
 *
 * Default rule uses the following specified error configuration:
 * [
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\validation\rule\api\RuleInterface;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\library\reflection\library\ToolBoxClassReflection;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\exception\ConfigInvalidFormatException;
use liberty_code\validation\rule\exception\DataNameInvalidFormatException;
use liberty_code\validation\rule\exception\ErrorConfigInvalidFormatException;
use liberty_code\validation\rule\exception\ValidationFailException;



abstract class DefaultRule extends FixBean implements RuleInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        if(!$this->beanExists(ConstRule::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstRule::DATA_KEY_DEFAULT_CONFIG] = $this->getTabFixConfig();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRule::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRule::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value, $this->getTabFixConfig());
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array.
     * Overwrite it to implement specific configuration.
     *
     * @return array
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array();
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstRule::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstRule::TAB_CONFIG_KEY_KEY, $tabConfig) ?
                $tabConfig[ConstRule::TAB_CONFIG_KEY_KEY] :
                ToolBoxClassReflection::getStrClassName(static::class)
        );

        // Return result
        return $result;
    }



    /**
     * Get string error message sprintf pattern.
     *
     * Configuration format:
     * Error configuration can be provided.
     * @see RuleInterface::getError() .
     *
     * @param array $tabConfig = null
     * @return null|string
     */
    protected function getStrErrorMsgPattern(array $tabConfig = null)
    {
        // Init var
        $tabErrorConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN, $tabConfig) ?
                $tabConfig[ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN] :
                null
        );
        $result = (
            (
                is_array($tabErrorConfig) &&
                array_key_exists(ConstRule::TAB_ERROR_CONFIG_KEY_ERROR_MESSAGE_PATTERN, $tabErrorConfig)
            ) ?
                $tabErrorConfig[ConstRule::TAB_ERROR_CONFIG_KEY_ERROR_MESSAGE_PATTERN] :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get string error message engine,
     * from specified string error message sprintf pattern
     * and specified data.
     * Overwrite it to implement specific feature.
     *
     * Configuration format:
     * Error configuration can be provided.
     * @see RuleInterface::getError() .
     *
     * @param string $strErrorMsgPattern
     * @param string $strName
     * @param mixed $value
     * @param array $tabConfig = null
     * @return null|string
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Init var
        $strValue = (
            ToolBoxString::checkConvertString($value) ?
                strval($value) :
                ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
        );
        $result = sprintf($strErrorMsgPattern, $strName, $strValue);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws DataNameInvalidFormatException
     * @throws ErrorConfigInvalidFormatException
     */
    public function getStrErrorMessage(
        $strName,
        $value,
        array $tabConfig = null
    )
    {
        // Set check argument
        DataNameInvalidFormatException::setCheck($strName);
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $strErrorMsgPattern = $this->getStrErrorMsgPattern($tabConfig);
        // Get error message, if required (error message pattern found)
        $result = (
            (!is_null($strErrorMsgPattern)) ?
                $this->getStrErrorMessageEngine(
                    $strErrorMsgPattern,
                    $strName,
                    $value,
                    $tabConfig
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws DataNameInvalidFormatException
     * @throws ErrorConfigInvalidFormatException
     */
    public function getObjErrorException(
        $strName,
        $value,
        array $tabConfig = null
    )
    {
        // Init var
        $result = null;
        $strErrorMsg = $this->getStrErrorMessage($strName, $value, $tabConfig);

        // Get error message, if required (error message found)
        if(!is_null($strErrorMsg))
        {
            $result = new ValidationFailException(
                $strErrorMsg,
                $this,
                $strName,
                $value,
                $tabConfig
            );
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        // Format configuration
        $tabConfig = array_merge($this->getTabFixConfig(), $tabConfig);

        // Set configuration
        $this->beanSet(ConstRule::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}