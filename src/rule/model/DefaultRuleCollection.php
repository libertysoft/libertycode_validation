<?php
/**
 * Description :
 * This class allows to define default rule collection class.
 * key: rule key => rule.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\validation\rule\api\RuleCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\validation\rule\api\RuleInterface;
use liberty_code\validation\rule\exception\CollectionKeyInvalidFormatException;
use liberty_code\validation\rule\exception\CollectionValueInvalidFormatException;



class DefaultRuleCollection extends DefaultBean implements RuleCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var RuleInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjRule($strKey)));
    }



	
	
	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Return result
        return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
    }



    /**
     * @inheritdoc
     */
    public function getObjRule($strKey)
    {
        // Init var
        $result = null;

        // Try to get rule object if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);;
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }
	




	// Methods setters
	// ******************************************************************************

	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setRule(RuleInterface $objRule)
	{
		// Init var
		$strKey = $objRule->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objRule);
		
		// return result
		return $strKey;
	}



    /**
     * @inheritdoc
     */
    public function setTabRule($tabRule)
    {
        // Init var
        $result = array();

        // Case index array of rules
        if(is_array($tabRule))
        {
            // Run all rules and for each, try to set
            foreach($tabRule as $rule)
            {
                $strKey = $this->setRule($rule);
                $result[] = $strKey;
            }
        }
        // Case collection of rules
        else if($tabRule instanceof RuleCollectionInterface)
        {
            // Run all rules and for each, try to set
            foreach($tabRule->getTabKey() as $strKey)
            {
                $objRule = $tabRule->getObjRule($strKey);
                $strKey = $this->setRule($objRule);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRule($strKey)
    {
        // Init var
        $result = $this->getObjRule($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRuleAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeRule($strKey);
        }
    }



}