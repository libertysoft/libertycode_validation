<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\standard\exception;

use Exception;

use liberty_code\validation\validator\standard\library\ConstStandardValidator;



class RuleNotFoundException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $name
     */
	public function __construct($name)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
        $this->message = sprintf(ConstStandardValidator::EXCEPT_MSG_RULE_NOT_FOUND, strval($name));
	}



}