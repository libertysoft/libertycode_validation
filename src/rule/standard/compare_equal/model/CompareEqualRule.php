<?php
/**
 * Description :
 * This class allows to define compare equal rule class.
 * Compare equal rule allows to check if specified data is equal to specified compare value.
 *
 * Compare equal rule uses the following specified configuration:
 * [
 *     Default rule configuration,
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by compare value"
 * ]
 *
 * Compare equal rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     compare_value(required): mixed value to compare
 * ]
 *
 * Compare equal rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     compare_value(required): mixed value to compare,
 *
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by compare value"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\compare_equal\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\compare_equal\library\ConstCompareEqualRule;
use liberty_code\validation\rule\standard\compare_equal\exception\ValidConfigInvalidFormatException;
use liberty_code\validation\rule\standard\compare_equal\exception\ErrorConfigInvalidFormatException;



class CompareEqualRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $compareValue = $tabConfig[ConstCompareEqualRule::TAB_VALID_CONFIG_KEY_COMPARE_VALUE];
            $result = (
                // Check value equal to compare value
                (
                    is_object($value) &&
                    is_object($compareValue) &&
                    ($value == $compareValue)
                ) ||
                (
                    (!is_object($value)) &&
                    ($value === $compareValue)
                )
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstCompareEqualRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstCompareEqualRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = null;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strValue = (
                ToolBoxString::checkConvertString($value) ?
                    strval($value) :
                    ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
            );
            $compareValue = $tabConfig[ConstCompareEqualRule::TAB_ERROR_CONFIG_KEY_COMPARE_VALUE];
            $strCompareValue = (
                ToolBoxString::checkConvertString($compareValue) ?
                    strval($compareValue) :
                    ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
            );
            $result = sprintf($strErrorMsgPattern, $strName, $strValue, $strCompareValue);
        }

        // Return result
        return $result;
    }



}