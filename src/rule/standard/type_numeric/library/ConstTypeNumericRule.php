<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\type_numeric\library;



class ConstTypeNumericRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'type_numeric';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s must be a valid number.';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_STRING_ENABLE_REQUIRE = 'string_enable_require';
    const TAB_VALID_CONFIG_KEY_FLOAT_ENABLE_REQUIRE = 'float_enable_require';
    const TAB_VALID_CONFIG_KEY_INTEGER_ONLY_REQUIRE = 'integer_only_require';


	
    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the type numeric rule validation configuration standard.';



}