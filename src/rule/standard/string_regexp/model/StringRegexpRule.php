<?php
/**
 * Description :
 * This class allows to define string REGEXP rule class.
 * String REGEXP rule allows to check if specified data matches with specified REGEXP pattern.
 *
 * String REGEXP rule uses the following specified configuration:
 * [
 *     Default rule configuration,
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by REGEXP pattern"
 * ]
 *
 * String REGEXP rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     regexp(required): string contain value
 * ]
 *
 * String REGEXP rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     regexp(required): string contain value,
 *
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name, '%2$s' replaced by data value, '%3$s' replaced by REGEXP pattern"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\string_regexp\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\string_regexp\library\ConstStringRegexpRule;
use liberty_code\validation\rule\standard\string_regexp\exception\ValidConfigInvalidFormatException;
use liberty_code\validation\rule\standard\string_regexp\exception\ErrorConfigInvalidFormatException;



class StringRegexpRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strPattern = $tabConfig[ConstStringRegexpRule::TAB_VALID_CONFIG_KEY_REGEXP];

            // Check value matches with REGEXP pattern
            $result = (
                is_string($value) &&
                (preg_match($strPattern, $value) === 1)
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstStringRegexpRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstStringRegexpRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = null;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Init var
            $strValue = (
                ToolBoxString::checkConvertString($value) ?
                    strval($value) :
                    ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
            );
            $strPattern = $tabConfig[ConstStringRegexpRule::TAB_ERROR_CONFIG_KEY_REGEXP];
            $result = sprintf($strErrorMsgPattern, $strName, $strValue, $strPattern);
        }

        // Return result
        return $result;
    }



}