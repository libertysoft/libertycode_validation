<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\compare_less\exception;

use Exception;

use DateTime;
use liberty_code\validation\rule\standard\compare_less\library\ConstCompareLessRule;



class ValidConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCompareLessRule::EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid compare value
            array_key_exists(ConstCompareLessRule::TAB_VALID_CONFIG_KEY_COMPARE_VALUE, $config) &&
            (
                is_string($config[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_COMPARE_VALUE]) ||
                is_numeric($config[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_COMPARE_VALUE]) ||
                ($config[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_COMPARE_VALUE] instanceof DateTime)
            ) &&

            // Check valid equal enable required option
            (
                (!isset($config[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_EQUAL_ENABLE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_EQUAL_ENABLE_REQUIRE]) ||
                    is_int($config[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_EQUAL_ENABLE_REQUIRE]) ||
                    (
                        is_string($config[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_EQUAL_ENABLE_REQUIRE]) &&
                        ctype_digit($config[ConstCompareLessRule::TAB_VALID_CONFIG_KEY_EQUAL_ENABLE_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}