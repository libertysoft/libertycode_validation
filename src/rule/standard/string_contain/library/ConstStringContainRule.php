<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\string_contain\library;



class ConstStringContainRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'string_contain';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s doesn\'t contain %3$s.';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_CONTAIN_VALUE = 'contain_value';
    const TAB_VALID_CONFIG_KEY_ENCODING_NAME = 'encoding_name';

    // Error configuration
    const TAB_ERROR_CONFIG_KEY_CONTAIN_VALUE = 'contain_value';


	
    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the string contain rule validation configuration standard.';
    const EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the string contain rule error configuration standard.';



}