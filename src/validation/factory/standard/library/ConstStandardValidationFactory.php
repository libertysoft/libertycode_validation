<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\factory\standard\library;



class ConstStandardValidationFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_VALIDATOR = 'objValidator';



    // Type configuration
    const CONFIG_TYPE_STANDARD = 'standard';



}