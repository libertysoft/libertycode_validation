<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load
require($strRootAppPath . '/test/validator/boot/ValidatorBootstrap.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\validation\validation\factory\standard\model\StandardValidationFactory;



// Init DI
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister);
$objProvider = new DefaultProvider($objDepCollection);

// Init var
$objValidationFactory = new StandardValidationFactory($objValidator, null, $objProvider);


