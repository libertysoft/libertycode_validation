<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\string_regexp\library;



class ConstStringRegexpRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'string_regexp';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s doesn\'t match with REGEXP %3$s.';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_REGEXP = 'regexp';

    // Error configuration
    const TAB_ERROR_CONFIG_KEY_REGEXP = 'regexp';


	
    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the string REGEXP rule validation configuration standard.';
    const EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the string REGEXP rule error configuration standard.';



}