<?php
/**
 * Description :
 * This class allows to describe behavior of rule class.
 * Rule allows to design a specific validation,
 * to validate specified data.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\api;

use Exception;



interface RuleInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * Check if specified data is valid.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * Null means no specific validation customization required.
	 * 
	 * @param string $strName
     * @param mixed $value
     * @param array $tabConfig = null
	 * @return boolean
	 */
	public function checkIsValid($strName, $value, array $tabConfig = null);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get string key (considered as rule id).
	 *
	 * @return string
	 */
	public function getStrKey();


	
    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get string error message,
     * from specified data.
     *
     * Configuration format:
     * Error configuration can be provided.
     * Null means no specific error customization required.
     *
     * @param string $strName
     * @param mixed $value
     * @param array $tabConfig = null
     * @return null|string
     */
    public function getStrErrorMessage(
        $strName,
        $value,
        array $tabConfig = null
    );



    /**
     * Get error exception object,
     * from specified data.
     *
     * Configuration format:
     * Error configuration can be provided.
     * @see RuleInterface::getStrErrorMessage() .
     *
     * @param string $strName
     * @param mixed $value
     * @param array $tabConfig = null
     * @return null|Exception
     */
    public function getObjErrorException(
        $strName,
        $value,
        array $tabConfig = null
    );
	
	


	
	// Methods setters
	// ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}