<?php
/**
 * Description :
 * This class allows to define type boolean rule class.
 * Type boolean rule allows to check if specified data is boolean.
 *
 * Type boolean rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Type boolean rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     integer_enable_require(optional: got true if not found): true / false,
 *
 *     string_enable_require(optional: got true if not found): true / false
 * ]
 *
 * Type boolean rule uses the following specified error configuration:
 * [
 *     Default rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\type_boolean\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\type_boolean\library\ConstTypeBooleanRule;
use liberty_code\validation\rule\standard\type_boolean\exception\ValidConfigInvalidFormatException;



class TypeBooleanRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check integer enable required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see TypeBooleanRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkIntegerEnableRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            (!is_array($tabValidConfig)) ||
            (!array_key_exists(ConstTypeBooleanRule::TAB_VALID_CONFIG_KEY_INTEGER_ENABLE_REQUIRE, $tabValidConfig)) ||
            (intval($tabValidConfig[ConstTypeBooleanRule::TAB_VALID_CONFIG_KEY_INTEGER_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check string enable required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see TypeBooleanRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkStringEnableRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            (!is_array($tabValidConfig)) ||
            (!array_key_exists(ConstTypeBooleanRule::TAB_VALID_CONFIG_KEY_STRING_ENABLE_REQUIRE, $tabValidConfig)) ||
            (intval($tabValidConfig[ConstTypeBooleanRule::TAB_VALID_CONFIG_KEY_STRING_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $boolIntEnable = $this->checkIntegerEnableRequired($tabConfig);
        $boolStrEnable = $this->checkStringEnableRequired($tabConfig);
        $result = (
            // Case boolean
            is_bool($value) ||

            // Case integer enable
            (
                $boolIntEnable &&
                is_int($value)
            ) ||

            // Case string enable
            (
                $boolStrEnable &&
                is_string($value) &&
                ctype_digit($value)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstTypeBooleanRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstTypeBooleanRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}