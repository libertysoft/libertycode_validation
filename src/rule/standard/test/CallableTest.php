<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/rule/standard/test/RuleCollectionTest.php');



// Test data validation
echo('Test validation : <br />');

$objDateBefore = new DateTime();
$objDateBefore->sub(new DateInterval('P1D'));
$objDate = new DateTime();
$objDateAfter = new DateTime();
$objDateAfter->add(new DateInterval('P1D'));
$tabInfo = array(
    // Test callable Ko
    [
        'callable',
        'key_1',
        'Value 1',
        null
    ],

    // Test callable Ok
    [
        'callable',
        'key_1',
        'Value 1',
        [
            'valid_callable' => function($strName, $value)
            {
                return is_string($value);
            }
        ]
    ],

    // Test callable Ko
    [
        'callable',
        'key_1',
        7,
        [
            'valid_callable' => function($strName, $value)
            {
                return is_string($value);
            }
        ]
    ],

    // Test callable Ko
    [
        'callable',
        'key_1',
        'Value 1',
        [
            'valid_callable' => function($strName, $value)
            {
                return is_numeric($value);
            },
            'error_message_callable' => function($strName, $value)
            {
                return sprintf('%1$s is not numeric!', $strName);
            }
        ]
    ],

    // Test callable Ok
    [
        'callable',
        'key_1',
        7,
        [
            'valid_callable' => function($strName, $value)
            {
                return is_numeric($value);
            },
            'error_message_callable' => function($strName, $value)
            {
                return sprintf('%1$s is not numeric!', $strName);
            }
        ]
    ]
);

foreach($tabInfo as $info)
{
    echo('Test validation info: <br />');

    try{
        // Get info
        $strKey = $info[0];
        $strName = $info[1];
        $value = $info[2];
        $tabConfig = $info[3];

        // Get rule
        $boolExists = $objRuleCollection->checkExists($strKey);
        $objRule = $objRuleCollection->getObjRule($strKey);

        echo('Info: <pre>');
        print_r(array(
            $strKey,
            $strName,
            (is_object($value) ? get_class($value) : $value),
            $tabConfig
        ));
        echo('</pre>');

        // Test validation, if required
        if($boolExists)
        {
            // Get validation
            $boolIsValid = $objRule->checkIsValid($strName, $value, $tabConfig);
            $strErrorMessage = $objRule->getStrErrorMessage($strName, $value, $tabConfig);
            $objErrorException = $objRule->getObjErrorException($strName, $value, $tabConfig);

            echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
            echo('Get error message: <pre>');var_dump($strErrorMessage);echo('</pre>');
            echo('Get error exception: <pre>');
            var_dump(
                (!is_null($objErrorException)) ?
                    get_class($objErrorException) . ': ' . $objErrorException->getMessage() :
                    $objErrorException
            );
            echo('</pre>');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


