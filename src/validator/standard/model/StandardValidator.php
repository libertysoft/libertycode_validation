<?php
/**
 * Description :
 * This class allows to define standard validator class.
 * Standard validator uses rule objects,
 * to validate data.
 *
 * Standard validator uses the following specified configuration:
 * [
 *     Default validator configuration
 * ]
 *
 * Note:
 * - Rule configuration interpretation, for specific data:
 *     - Each rule name is used as rule key, to select specific rule object,
 *     from rule collection.
 *     - Each rule configuration is used as validation configuration and error configuration,
 *     on selected rule object.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\standard\model;

use liberty_code\validation\validator\library\ConstValidator;
use liberty_code\validation\validator\model\DefaultValidator;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\validation\rule\api\RuleInterface;
use liberty_code\validation\rule\api\RuleCollectionInterface;
use liberty_code\validation\validator\standard\library\ConstStandardValidator;
use liberty_code\validation\validator\standard\exception\RuleCollectionInvalidFormatException;
use liberty_code\validation\validator\standard\exception\RuleNotFoundException;



/**
 * @method RuleCollectionInterface getObjRuleCollection() Get rule collection object.
 * @method void setObjRuleCollection(RuleCollectionInterface $objRuleCollection) Set rule collection object.
 */
class StandardValidator extends DefaultValidator
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RuleCollectionInterface $objRuleCollection
     */
    public function __construct(
        RuleCollectionInterface $objRuleCollection,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $objCacheRepo
        );

        // Init rule collection
        $this->setObjRuleCollection($objRuleCollection);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStandardValidator::DATA_KEY_DEFAULT_RULE_COLLECTION))
        {
            $this->__beanTabData[ConstStandardValidator::DATA_KEY_DEFAULT_RULE_COLLECTION] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStandardValidator::DATA_KEY_DEFAULT_RULE_COLLECTION
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStandardValidator::DATA_KEY_DEFAULT_RULE_COLLECTION:
                    RuleCollectionInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws RuleNotFoundException
     */
    protected function checkDataIsValidEngine(
        $strName,
        $value,
        array $tabRuleConfig,
        array &$tabErrorMessage = array(),
        array &$tabErrorException = array()
    )
    {
        // Init var
        $result = true;
        $tabErrorMessage = array();
        $tabErrorException = array();

        // Run each rule configuration
        foreach($tabRuleConfig as $ruleConfig)
        {
            // Get info
            $strRuleNm = $ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_NAME];
            $ruleConfig = (
                isset($ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_CONFIG]) ?
                    $ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_CONFIG] :
                    null
            );

            // Get rule
            $objRule = $this->getObjRuleFromName($strRuleNm);

            // Check is valid
            $boolIsValid = $objRule->checkIsValid($strName, $value, $ruleConfig);
            $result = $result && $boolIsValid;

            // Get error, if required
            if(!$boolIsValid)
            {
                $tabErrorMessage[$strRuleNm] = $objRule->getStrErrorMessage($strName, $value, $ruleConfig);
                $tabErrorException[$strRuleNm] = $objRule->getObjErrorException($strName, $value, $ruleConfig);
            }
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get rule object,
     * from specified rule name.
     *
     * @param string $strName
     * @return RuleInterface
     * @throws RuleNotFoundException
     */
    public function getObjRuleFromName($strName)
    {
        // Init var
        $objRuleCollection = $this->getObjRuleCollection();
        $result = (
            is_string($strName) ?
                $objRuleCollection->getObjRule($strName) :
                null
        );

        // Check rule found
        if(is_null($result))
        {
            throw new RuleNotFoundException($strName);
        }

        // Return result
        return $result;
    }



}