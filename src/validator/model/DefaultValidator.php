<?php
/**
 * Description :
 * This class allows to define default validator class.
 * Can be consider is base of all validator types.
 *
 * Default validator uses the following specified configuration:
 * [
 *     cache_require(optional: got true, if not found): true / false,
 *
 *     cache_key_pattern(optional: got @see ConstValidator::CACHE_DEFAULT_KEY_PATTERN, if not found):
 *         "string sprintf pattern,
 *         to build key used on cache repository,
 *         where '%1$s', '%2$s' and '%3$s',
 *         replaced by specified data name, specified date value hash and specified rule configuration hash",
 *
 *     cache_value_is_valid_key(optional: got @see ConstValidator::CACHE_DEFAULT_VALUE_IS_VALID_KEY, if not found):
 *         "string key,
 *         used on cache repository,
 *         to store validation",
 *
 *     cache_value_error_message_key(optional: got @see ConstValidator::CACHE_DEFAULT_VALUE_ERROR_MESSAGE_KEY, if not found):
 *         "string key,
 *         used on cache repository,
 *         to store error messages",
 *
 *     cache_value_error_exception_key(optional: got @see ConstValidator::CACHE_DEFAULT_VALUE_ERROR_EXCEPTION_KEY, if not found):
 *         "string key,
 *         used on cache repository,
 *         to store error exceptions",
 *
 *     cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\validation\validator\api\ValidatorInterface;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\validation\validator\library\ConstValidator;
use liberty_code\validation\validator\library\ToolBoxRuleConfig;
use liberty_code\validation\validator\exception\ConfigInvalidFormatException;
use liberty_code\validation\validator\exception\CacheRepoInvalidFormatException;
use liberty_code\validation\validator\exception\DataNameInvalidFormatException;
use liberty_code\validation\validator\exception\RuleConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method null|RepositoryInterface getObjCacheRepo() Get cache repository object.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 * @method void setObjCacheRepo(null|RepositoryInterface $objCacheRepo) Set cache repository object.
 */
abstract class DefaultValidator extends FixBean implements ValidatorInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param RepositoryInterface $objCacheRepo = null
     */
    public function __construct(
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init cache repository, if required
        if(!is_null($objCacheRepo))
        {
            $this->setObjCacheRepo($objCacheRepo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstValidator::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstValidator::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstValidator::DATA_KEY_DEFAULT_CACHE_REPO))
        {
            $this->__beanTabData[ConstValidator::DATA_KEY_DEFAULT_CACHE_REPO] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstValidator::DATA_KEY_DEFAULT_CONFIG,
            ConstValidator::DATA_KEY_DEFAULT_CACHE_REPO
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstValidator::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstValidator::DATA_KEY_DEFAULT_CACHE_REPO:
                    CacheRepoInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * @return boolean
     */
    public function checkCacheRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjCacheRepo())) &&
            (
                (!isset($tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check if specified cache value,
     * from specific data validation,
     * is valid.
     * Overwrite it to set specific feature.
     *
     * Error messages array format:
     * @see checkDataIsValid() error messages array format.
     *
     * Error exceptions array format:
     * @see checkDataIsValid() error exceptions array format.
     *
     * @param mixed $value
     * @param null|array &$tabErrorMessage
     * @param null|array &$tabErrorException
     * @return null|boolean
     */
    protected function checkCacheValueIsValid(
        $value,
        &$tabErrorMessage,
        &$tabErrorException
    )
    {
        // Init var
        $result = null;
        $tabErrorMessage = null;
        $tabErrorException = null;
        $strIsValidKey = $this->getStrCacheValueIsValidKey();
        $strErrMsgKey = $this->getStrCacheValueErrMsgKey();
        $strErrExceptKey = $this->getStrCacheValueErrExceptKey();

        // Get validation, if required
        $tabCacheValue = (is_array($value) ? $value : null);
        if(
            isset($tabCacheValue[$strIsValidKey]) && is_bool($tabCacheValue[$strIsValidKey]) &&
            isset($tabCacheValue[$strErrMsgKey]) && is_array($tabCacheValue[$strErrMsgKey]) &&
            isset($tabCacheValue[$strErrExceptKey]) && is_array($tabCacheValue[$strErrExceptKey])
        )
        {
            $result = $tabCacheValue[$strIsValidKey];
            $tabErrorMessage = $tabCacheValue[$strErrMsgKey];
            $tabErrorException = $tabCacheValue[$strErrExceptKey];
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified data is valid engine,
     * from specified rule configuration.
     * Errors can be returned,
     * with all failed rules names.
     *
     * Rule configuration array format:
     * @see getTabFormatRuleConfig() return array format.
     *
     * Error messages array format:
     * @see checkDataIsValid() error messages array format.
     *
     * Error exceptions array format:
     * @see checkDataIsValid() error exceptions array format.
     *
     * @param string $strName
     * @param mixed $value
     * @param array $tabRuleConfig
     * @param array &$tabErrorMessage = array()
     * @param array &$tabErrorException = array()
     * @return boolean
     */
    abstract protected function checkDataIsValidEngine(
        $strName,
        $value,
        array $tabRuleConfig,
        array &$tabErrorMessage = array(),
        array &$tabErrorException = array()
    );



    /**
     * @inheritdoc
     * @throws DataNameInvalidFormatException
     * @throws RuleConfigInvalidFormatException
     */
    public function checkDataIsValid(
        $strName,
        $value,
        array $tabRuleConfig,
        array &$tabErrorMessage = array(),
        array &$tabErrorException = array()
    )
    {
        // Set check argument
        DataNameInvalidFormatException::setCheck($strName);

        // Init var
        $result = true;
        $tabRuleConfig = $this->getTabFormatRuleConfig($tabRuleConfig);
        $tabConfig = $this->getTabConfig();
        $objCacheRepo = $this->getObjCacheRepo();
        $boolCacheRequired = $this->checkCacheRequired();
        $strCacheKey = (
            (!is_null($boolCacheRequired)) ?
                $this->getStrCacheKey($strName, $value, $tabRuleConfig) :
                null
        );
        $boolProcess = true;

        // Get data validation from cache, if required
        if($boolCacheRequired)
        {
            $cacheValue = $objCacheRepo->getItem($strCacheKey);
            $tabErrorMessage = null;
            $tabErrorException = null;
            $result = (
                (!is_null($cacheValue)) ?
                    $this->checkCacheValueIsValid(
                        $cacheValue,
                        $tabErrorMessage,
                        $tabErrorException
                    ) :
                    null
            );
            $boolProcess = (
                (!is_bool($result)) ||
                (!is_array($tabErrorMessage)) ||
                (!is_array($tabErrorException))
            );
        }

        // Get data validation, if required
        if($boolProcess)
        {
            // Get data validation
            $tabErrorMessage = array();
            $tabErrorException = array();
            $result = $this->checkDataIsValidEngine(
                $strName,
                $value,
                $tabRuleConfig,
                $tabErrorMessage,
                $tabErrorException
            );

            // Set data validation on cache, if required
            if($boolCacheRequired)
            {
                // Get cache value
                $cacheValue = $this->getCacheValue(
                    $result,
                    $tabErrorMessage,
                    $tabErrorException
                );

                // Set cache data
                $objCacheRepo->setItem(
                    $strCacheKey,
                    $cacheValue,
                    (
                        isset($tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                );
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified data are valid engine,
     * from specified rule configurations.
     * Errors can be provided,
     * with all failed data and failed rules names.
     *
     * Data array format:
     * @see checkTabDataIsValid() data array format.
     *
     * Rule configurations array format:
     * [key: data name => value: @see getTabFormatRuleConfig() return array format]
     *
     * Errors messages array format:
     * @see checkTabDataIsValid() error messages array format.
     *
     * Errors exceptions array format:
     * @see checkTabDataIsValid() error exceptions array format.
     *
     * @param array $tabData
     * @param array $tabRuleConfig
     * @param array &$tabErrorMessage = array()
     * @param array &$tabErrorException = array()
     * @return boolean
     */
    protected function checkTabDataIsValidEngine(
        array $tabData,
        array $tabRuleConfig,
        array &$tabErrorMessage = array(),
        array &$tabErrorException = array()
    )
    {
        // Init var
        $result = true;
        $tabErrorMessage = array();
        $tabErrorException = array();

        // Run each data
        foreach($tabData as $strName => $value)
        {
            // Get info
            $tabDataRuleConfig = (isset($tabRuleConfig[$strName]) ? $tabRuleConfig[$strName] : array());
            $tabDataErrorMessage = array();
            $tabDataErrorException = array();

            // Check data is valid
            $boolDataIsValid = $this->checkDataIsValidEngine(
                $strName,
                $value,
                $tabDataRuleConfig,
                $tabDataErrorMessage,
                $tabDataErrorException
            );
            $result = $result && $boolDataIsValid;

            // Get error, if required
            if(!$boolDataIsValid)
            {
                $tabErrorMessage[$strName] = $tabDataErrorMessage;
                $tabErrorException[$strName] = $tabDataErrorException;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws DataNameInvalidFormatException
     * @throws RuleConfigInvalidFormatException
     */
    public function checkTabDataIsValid(
        array $tabData,
        array $tabRuleConfig,
        array &$tabErrorMessage = array(),
        array &$tabErrorException = array()
    )
    {
        // Set check argument
        foreach(array_keys($tabData) as $strName)
        {
            DataNameInvalidFormatException::setCheck($strName);
        }

        foreach($tabRuleConfig as $strName => $tabDataRuleConfig)
        {
            DataNameInvalidFormatException::setCheck($strName);

            $tabRuleConfig[$strName] = $this->getTabFormatRuleConfig($tabDataRuleConfig);
        }

        // Init var
        $result = true;
        $tabErrorMessage = array();
        $tabErrorException = array();
        $tabConfig = $this->getTabConfig();
        $objCacheRepo = $this->getObjCacheRepo();
        $boolCacheRequired = $this->checkCacheRequired();
        $tabCacheKey = array();

        // Get data validations to process, data validations from cache, if required
        $tabProcessData = $tabData;
        if($boolCacheRequired) {
            $tabProcessData = array();

            // Get cache keys
            foreach($tabData as $strName => $value)
            {
                $tabDataRuleConfig = (isset($tabRuleConfig[$strName]) ? $tabRuleConfig[$strName] : array());
                $tabCacheKey[$strName] = $this->getStrCacheKey($strName, $value, $tabDataRuleConfig);
            }

            // Get cache values
            $tabCacheValue = $objCacheRepo->getTabItem(array_values($tabCacheKey));

            // Get data validations to process, data validations from cache
            foreach($tabData as $strName => $value)
            {
                // Get cache info
                $strCacheKey = $tabCacheKey[$strName];
                $cacheValue = isset($tabCacheValue[$strCacheKey]) ? $tabCacheValue[$strCacheKey] : null;

                // Get data validation, from cache
                $tabDataErrorMessage = null;
                $tabDataErrorException = null;
                $boolDataIsValid = (
                    (!is_null($cacheValue)) ?
                        $this->checkCacheValueIsValid(
                            $cacheValue,
                            $tabDataErrorMessage,
                            $tabDataErrorException
                        ) :
                        null
                );

                // Set result with data validation from cache, if required
                if(
                    is_bool($boolDataIsValid) &&
                    is_array($tabDataErrorMessage) &&
                    is_array($tabDataErrorException)
                )
                {
                    $result = $result && $boolDataIsValid;

                    if(!$boolDataIsValid)
                    {
                        $tabErrorMessage[$strName] = $tabDataErrorMessage;
                        $tabErrorException[$strName] = $tabDataErrorException;
                    }
                }
                // Case else: set data validation to process
                else
                {
                    $tabProcessData[$strName] = $value;
                }
            }
        }

        // Get data validations
        $tabProcessErrorMessage = array();
        $tabProcessErrorException = array();
        $result = $this->checkTabDataIsValidEngine(
            $tabProcessData,
            $tabRuleConfig,
            $tabProcessErrorMessage,
            $tabProcessErrorException
        ) && $result;
        $tabErrorMessage = array_merge($tabErrorMessage, $tabProcessErrorMessage);
        $tabErrorException = array_merge($tabErrorException, $tabProcessErrorException);

        // Set data validations on cache, if required
        if($boolCacheRequired)
        {
            // Get cache data
            $tabCacheData = array();
            foreach($tabProcessData as $strName => $value)
            {
                // Get cache keys
                $strCacheKey = $tabCacheKey[$strName];

                // Get cache values
                $boolDataIsValid = (
                    (!array_key_exists($strName, $tabProcessErrorMessage)) &&
                    (!array_key_exists($strName, $tabProcessErrorException))
                );
                $tabDataErrorMessage = ((!$boolDataIsValid) ? $tabProcessErrorMessage[$strName] : array());
                $tabDataErrorException = ((!$boolDataIsValid) ? $tabProcessErrorException[$strName] : array());
                $cacheValue = $this->getCacheValue(
                    $boolDataIsValid,
                    $tabDataErrorMessage,
                    $tabDataErrorException
                );

                // Get cache data
                $tabCacheData[$strCacheKey] = $cacheValue;
            }

            // Set cache data
            $objCacheRepo->setTabItem(
                $tabCacheData,
                (
                    isset($tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                        $tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                        null
                )
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified formatted rule configuration array.
     *
     * Rule configuration array format:
     * @see checkDataIsValid() rule configuration array format.
     *
     * Return array format:
     * @see ToolBoxRuleConfig::getTabFormatRuleConfig() return array format.
     *
     * @param mixed $ruleConfig
     * @return array
     * @throws RuleConfigInvalidFormatException
     */
    protected function getTabFormatRuleConfig($ruleConfig)
    {
        // Set check argument
        RuleConfigInvalidFormatException::setCheck($ruleConfig);

        // Return result
        return ToolBoxRuleConfig::getTabFormatRuleConfig($ruleConfig);
    }



    /**
     * Get specified cache key,
     * from specified data,
     * and specified rule configuration.
     *
     * Rule configuration array format:
     * @see checkDataIsValidEngine() rule configuration array format.
     *
     * @param string $strName
     * @param mixed $value
     * @param array $tabRuleConfig
     * @return null|string
     */
    protected function getStrCacheKey(
        $strName,
        $value,
        array $tabRuleConfig
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strPattern = (
            isset($tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_KEY_PATTERN]) ?
                $tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_KEY_PATTERN] :
                ConstValidator::CACHE_DEFAULT_KEY_PATTERN
        );
        $result = (
            (
                is_string($strName) &&
                is_string($strValueHash = ToolBoxHash::getStrHash($value)) &&
                is_string($strRuleConfigHash =  ToolBoxHash::getStrHash($tabRuleConfig))
            ) ?
                sprintf(
                    $strPattern,
                    $strName,
                    $strValueHash,
                    $strRuleConfigHash
                ) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get cache value validation key.
     *
     * @return string
     */
    protected function getStrCacheValueIsValidKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_IS_VALID_KEY]) ?
                $tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_IS_VALID_KEY] :
                ConstValidator::CACHE_DEFAULT_VALUE_IS_VALID_KEY
        );

        // Return result
        return $result;
    }



    /**
     * Get cache value error message key.
     *
     * @return string
     */
    protected function getStrCacheValueErrMsgKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_MESSAGE_KEY]) ?
                $tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_MESSAGE_KEY] :
                ConstValidator::CACHE_DEFAULT_VALUE_ERROR_MESSAGE_KEY
        );

        // Return result
        return $result;
    }



    /**
     * Get cache value error exception key.
     *
     * @return string
     */
    protected function getStrCacheValueErrExceptKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_EXCEPTION_KEY]) ?
                $tabConfig[ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_EXCEPTION_KEY] :
                ConstValidator::TAB_CONFIG_KEY_CACHE_VALUE_ERROR_EXCEPTION_KEY
        );

        // Return result
        return $result;
    }



    /**
     * Get cache value,
     * from specified data validation.
     * Overwrite it to set specific feature.
     *
     * Errors messages array format:
     * @see checkDataIsValid() error messages array format.
     *
     * Errors exceptions array format:
     * @see checkDataIsValid() error exceptions array format.
     *
     * @param boolean $boolIsValid
     * @param array $tabErrorMessage
     * @param array $tabErrorException
     * @return mixed
     */
    protected function getCacheValue(
        $boolIsValid,
        array $tabErrorMessage,
        array $tabErrorException
    )
    {
        // Init var
        $strIsValidKey = $this->getStrCacheValueIsValidKey();
        $strErrMsgKey = $this->getStrCacheValueErrMsgKey();
        $strErrExceptKey = $this->getStrCacheValueErrExceptKey();
        $result = array(
            $strIsValidKey => $boolIsValid,
            $strErrMsgKey => $tabErrorMessage,
            $strErrExceptKey => $tabErrorException
        );

        // Return result
        return $result;
    }



}