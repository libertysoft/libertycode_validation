<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\type_array\exception;

use Exception;

use liberty_code\validation\rule\standard\type_array\library\ConstTypeArrayRule;



class ValidConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstTypeArrayRule::EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid index only required option
            (
                (!isset($config[ConstTypeArrayRule::TAB_VALID_CONFIG_KEY_INDEX_ONLY_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstTypeArrayRule::TAB_VALID_CONFIG_KEY_INDEX_ONLY_REQUIRE]) ||
                    is_int($config[ConstTypeArrayRule::TAB_VALID_CONFIG_KEY_INDEX_ONLY_REQUIRE]) ||
                    (
                        is_string($config[ConstTypeArrayRule::TAB_VALID_CONFIG_KEY_INDEX_ONLY_REQUIRE]) &&
                        ctype_digit($config[ConstTypeArrayRule::TAB_VALID_CONFIG_KEY_INDEX_ONLY_REQUIRE])
                    )
                )
            ) ;

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}