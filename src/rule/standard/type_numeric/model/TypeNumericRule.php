<?php
/**
 * Description :
 * This class allows to define type numeric rule class.
 * Type numeric rule allows to check if specified data is numeric.
 *
 * Type numeric rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Type numeric rule uses the following specified validation configuration:
 * [
 *     Default rule configuration,
 *
 *     string_enable_require(optional: got true if not found): true / false,
 *
 *     float_enable_require(optional: got true if not found): true / false,
 *
 *     integer_only_require(optional: got false if not found):
 *         true / false :
 *         In case of string, each characters must be digit.
 *         In case of float, can't have decimal.
 * ]
 *
 * Type numeric rule uses the following specified error configuration:
 * [
 *     Default rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\type_numeric\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\standard\type_numeric\library\ConstTypeNumericRule;
use liberty_code\validation\rule\standard\type_numeric\exception\ValidConfigInvalidFormatException;



class TypeNumericRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check string enable required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see TypeNumericRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkStringEnableRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            (!is_array($tabValidConfig)) ||
            (!array_key_exists(ConstTypeNumericRule::TAB_VALID_CONFIG_KEY_STRING_ENABLE_REQUIRE, $tabValidConfig)) ||
            (intval($tabValidConfig[ConstTypeNumericRule::TAB_VALID_CONFIG_KEY_STRING_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check float enable required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see TypeNumericRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkFloatEnableRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            (!is_array($tabValidConfig)) ||
            (!array_key_exists(ConstTypeNumericRule::TAB_VALID_CONFIG_KEY_FLOAT_ENABLE_REQUIRE, $tabValidConfig)) ||
            (intval($tabValidConfig[ConstTypeNumericRule::TAB_VALID_CONFIG_KEY_FLOAT_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check integer only required.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see TypeNumericRule
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkIntegerOnlyRequired(array $tabConfig = null)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = (
            is_array($tabValidConfig) &&
            array_key_exists(ConstTypeNumericRule::TAB_VALID_CONFIG_KEY_INTEGER_ONLY_REQUIRE, $tabValidConfig) &&
            (intval($tabValidConfig[ConstTypeNumericRule::TAB_VALID_CONFIG_KEY_INTEGER_ONLY_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $boolStrEnable = $this->checkStringEnableRequired($tabConfig);
        $boolFloatEnable = $this->checkFloatEnableRequired($tabConfig);
        $boolIntOnly = $this->checkIntegerOnlyRequired($tabConfig);
        $result = (
            // Check numeric
            is_numeric($value) &&

            // Check integer only, if required
            (
                (!$boolIntOnly) ||
                is_int($value) ||
                (is_string($value) && ctype_digit($value)) ||
                (is_float($value) && (floor($value) == $value))
            ) &&

            // Check not string, if required
            (
                $boolStrEnable ||
                (!is_string($value))
            ) &&

            // Check not float, if required
            (
                $boolFloatEnable ||
                (!is_float($value))
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstTypeNumericRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstTypeNumericRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}