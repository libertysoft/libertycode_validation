<?php
/**
 * Description :
 * This class allows to define size sub-rule class.
 * Size sub-rule allows to check if specified data size passes specified rule configuration.
 *
 * Size sub-rule uses the following specified configuration:
 * [
 *     Sub-rule configuration
 * ]
 *
 * Size sub-rule uses the following specified validation configuration:
 * [
 *     Sub-rule validation configuration
 * ]
 *
 * Size sub-rule uses the following specified error configuration:
 * [
 *     Sub-rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\sub_rule\size\model;

use liberty_code\validation\validator\rule\sub_rule\model\SubRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\rule\sub_rule\size\library\ConstSizeSubRule;



class SizeSubRule extends SubRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(ValidatorInterface $objValidator)
    {
        // Call parent constructor
        parent::__construct($objValidator);
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabRuleConfig,
        array $tabConfig
    )
    {
        // Init var
        $objValidator = $this->getObjValidator();
        $intSize = (
            is_string($value) ?
                mb_strlen($value) :
                (
                    is_array($value) ?
                        count($value) :
                        (
                            (is_numeric($value)) ?
                                intval($value) :
                                null
                        )
                )
        );
        $result = (
            (!is_null($intSize)) &&
            $objValidator->checkDataIsValid($strName, $intSize, $tabRuleConfig)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstSizeSubRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstSizeSubRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}