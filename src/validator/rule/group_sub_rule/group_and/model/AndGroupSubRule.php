<?php
/**
 * Description :
 * This class allows to define and group sub-rule class.
 * And group sub-rule allows to check if specified data passes all specified rule configurations.
 *
 * And group sub-rule uses the following specified configuration:
 * [
 *     Group rule configuration
 * ]
 *
 * And group sub-rule uses the following specified validation configuration:
 * [
 *     Group rule validation configuration
 * ]
 *
 * And group sub-rule uses the following specified error configuration:
 * [
 *     Group rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\group_sub_rule\group_and\model;

use liberty_code\validation\validator\rule\group_sub_rule\model\GroupSubRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\rule\group_sub_rule\group_and\library\ConstAndGroupSubRule;



class AndGroupSubRule extends GroupSubRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(ValidatorInterface $objValidator)
    {
        // Call parent constructor
        parent::__construct($objValidator);
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabRuleConfig
    )
    {
        // Init var
        $result = true;
        $tabRuleConfigNm = array_keys($tabRuleConfig);
        $objValidator = $this->getObjValidator();

        // Run each rule configuration
        for($intCpt = 0; ($intCpt < count($tabRuleConfigNm)) && $result; $intCpt++)
        {
            $strRuleConfigNm = $tabRuleConfigNm[$intCpt];
            $ruleConfig = $tabRuleConfig[$strRuleConfigNm];

            // Check data passes rule configuration
            $result = $objValidator->checkDataIsValid($strName, $value, $ruleConfig);
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstAndGroupSubRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstAndGroupSubRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}