<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\library;



class ConstValidatorRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_VALIDATOR = 'objValidator';



    // Exception message constants
    const EXCEPT_MSG_VALIDATOR_INVALID_FORMAT = 'Following validator "%1$s" invalid! It must be a validator object.';



}