<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\exception;

use Exception;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\api\RuleInterface;



class ValidationFailException extends Exception
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var RuleInterface */
    protected $objRule;



    /**
     * Data name.
     *
     * @var string
     */
    protected $strName;



    /**
     * Data value.
     *
     * @var mixed
     */
    protected $value;



    /**
     * Validation configuration.
     *
     * @var null|array
     */
    protected $tabConfig;



    /**
     * Additive information.
     *
     * @var array
     */
    protected $tabInfo;





	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     *
	 * @param string $strErrorMsg
     * @param RuleInterface $objRule
     * @param string $strName
     * @param mixed $value
     * @param null|array $tabConfig = null
     * @param array $tabInfo = array()
     */
	public function __construct(
        $strErrorMsg,
        RuleInterface $objRule,
        $strName,
        $value,
        array $tabConfig = null,
        array $tabInfo = array()
    )
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$strErrorMsg =
			(is_string($strErrorMsg) && (trim($strErrorMsg) != '')) ?
				' ' . trim($strErrorMsg) :
				'';
		$this->message = sprintf(ConstRule::EXCEPT_MSG_VALIDATION_FAIL, $strErrorMsg);
		$this->objRule = $objRule;
        $this->strName = (is_string($strName) ? $strName : '');
        $this->value = $value;
        $this->tabConfig = $tabConfig;
        $this->tabInfo = $tabInfo;
	}





    // Methods getters
    // ******************************************************************************

    /**
     * Get rule object.
     *
     * @return RuleInterface
     */
    public function getObjRule()
    {
        // Return result
        return $this->objRule;
    }



    /**
     * Get data name.
     *
     * @return string
     */
    public function getStrName()
    {
        // Return result
        return $this->strName;
    }



    /**
     * Get data value.
     *
     * @return mixed
     */
    public function getValue()
    {
        // Return result
        return $this->value;
    }



    /**
     * Get validation configuration array.
     *
     * @return null|array
     */
    public function getTabConfig()
    {
        // Return result
        return $this->tabConfig;
    }



    /**
     * Get additive information array.
     *
     * @return array
     */
    public function getTabInfo()
    {
        // Return result
        return $this->tabInfo;
    }



}