<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\exception;

use Exception;

use liberty_code\validation\validator\library\ConstValidator;



class RuleConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(
            ConstValidator::EXCEPT_MSG_RULE_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkConfigIsValid($config)
    {
        // Init var
        $result = is_array($config);

        if($result) {
            $config = array_values($config);

            // Check each rule is valid
            for($intCpt = 0; $result && ($intCpt < count($config)); $intCpt++)
            {
                $ruleConfig = $config[$intCpt];
                $result =
                    // Check valid rule name
                    (
                        is_string($ruleConfig) &&
                        (trim($ruleConfig) != '')
                    ) ||

                    // Case array
                    (
                        is_array($ruleConfig) &&

                        // Check valid rule name
                        (
                            (
                                isset($ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_NAME]) &&
                                is_string($ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_NAME]) &&
                                (trim($ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_NAME]) != '')
                            ) ||
                            (
                                isset($ruleConfig[0]) &&
                                is_string($ruleConfig[0]) &&
                                (trim($ruleConfig[0]) != '')
                            )
                        ) &&

                        // Check valid rule configuration
                        (
                            (
                                (!isset($ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_CONFIG])) ||
                                is_array($ruleConfig[ConstValidator::TAB_RULE_CONFIG_KEY_RULE_CONFIG])
                            ) ||
                            (
                                (!isset($ruleConfig[1])) ||
                                is_array($ruleConfig[1])
                            )
                        )
                    );
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result = static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}