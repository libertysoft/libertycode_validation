<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validation\standard\exception;

use Exception;

use liberty_code\validation\validator\exception\DataNameInvalidFormatException;
use liberty_code\validation\validator\exception\RuleConfigInvalidFormatException;
use liberty_code\validation\validation\standard\library\ConstStandardValidation;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStandardValidation::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init data array check function
        $checkTabDataIsValid = function($tabData)
        {
            $result = is_array($tabData);

            // Check each data
            if($result)
            {
                $tabDataName = array_keys($tabData);
                for($intCpt = 0; ($intCpt < count($tabDataName)) && $result; $intCpt++)
                {
                    $strDataName = $tabDataName[$intCpt];
                    $result = (
                        // Check valid name
                        DataNameInvalidFormatException::checkNameIsValid($strDataName)
                    );
                }
            }

            return $result;
        };

        // Init rule configurations array check function
        $checkTabRuleConfigIsValid = function($tabRuleConfig)
        {
            $result = is_array($tabRuleConfig);

            // Check each data
            if($result)
            {
                $tabDataName = array_keys($tabRuleConfig);
                for($intCpt = 0; ($intCpt < count($tabDataName)) && $result; $intCpt++)
                {
                    $strDataName = $tabDataName[$intCpt];
                    $tabDataRuleConfig = $tabRuleConfig[$strDataName];
                    $result = (
                        // Check valid name
                        DataNameInvalidFormatException::checkNameIsValid($strDataName) &&

                        // Check valid rule configuration
                        RuleConfigInvalidFormatException::checkConfigIsValid($tabDataRuleConfig)
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid data
            (
                (!isset($config[ConstStandardValidation::TAB_CONFIG_KEY_DATA])) ||
                $checkTabDataIsValid($config[ConstStandardValidation::TAB_CONFIG_KEY_DATA])
            ) &&

            // Check valid rule configurations
            (
                (!isset($config[ConstStandardValidation::TAB_CONFIG_KEY_RULE_CONFIG])) ||
                $checkTabRuleConfigIsValid($config[ConstStandardValidation::TAB_CONFIG_KEY_RULE_CONFIG])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}