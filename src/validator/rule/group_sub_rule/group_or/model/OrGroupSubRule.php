<?php
/**
 * Description :
 * This class allows to define or group sub-rule class.
 * Or group sub-rule allows to check if specified data passes at least one of specified rule configurations.
 *
 * Or group sub-rule uses the following specified configuration:
 * [
 *     Group rule configuration
 * ]
 *
 * Or group sub-rule uses the following specified validation configuration:
 * [
 *     Group rule validation configuration
 * ]
 *
 * Or group sub-rule uses the following specified error configuration:
 * [
 *     Group rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\group_sub_rule\group_or\model;

use liberty_code\validation\validator\rule\group_sub_rule\model\GroupSubRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\rule\group_sub_rule\group_or\library\ConstOrGroupSubRule;



class OrGroupSubRule extends GroupSubRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(ValidatorInterface $objValidator)
    {
        // Call parent constructor
        parent::__construct($objValidator);
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabRuleConfig
    )
    {
        // Init var
        $result = false;
        $tabRuleConfigNm = array_keys($tabRuleConfig);
        $objValidator = $this->getObjValidator();

        // Run each rule configuration
        for($intCpt = 0; ($intCpt < count($tabRuleConfigNm)) && (!$result); $intCpt++)
        {
            $strRuleConfigNm = $tabRuleConfigNm[$intCpt];
            $ruleConfig = $tabRuleConfig[$strRuleConfigNm];

            // Check data passes rule configuration
            $result = $objValidator->checkDataIsValid($strName, $value, $ruleConfig);
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstOrGroupSubRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstOrGroupSubRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}