<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/rule/test/StringRuleTest.php');
require_once($strRootAppPath . '/src/rule/test/IntegerRuleTest.php');
require_once($strRootAppPath . '/src/rule/test/BetweenRuleTest.php');

// Use
use liberty_code\validation\rule\model\DefaultRuleCollection;

// Use test
use liberty_code\validation\rule\test\StringRuleTest;
use liberty_code\validation\rule\test\IntegerRuleTest;
use liberty_code\validation\rule\test\BetweenRuleTest;



// Init var
$objStringRule = new StringRuleTest();
$objIntegerRule = new IntegerRuleTest();
$objBetweenRule = new BetweenRuleTest();

$objRuleCollection = new DefaultRuleCollection();



// Add rule
$tabRule = array(
    $objStringRule,
    $objIntegerRule,
    $objBetweenRule
);
$objRuleCollection->setTabRule($tabRule);


