<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\rule\standard\type_object\library;



class ConstTypeObjectRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'type_object';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s must be a valid object.';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_CLASS_PATH = 'class_path';
    const TAB_VALID_CONFIG_KEY_EXTEND_ENABLE_REQUIRE = 'extend_enable_require';


	
    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the type object rule validation configuration standard.';



}