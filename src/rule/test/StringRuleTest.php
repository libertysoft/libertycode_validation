<?php

namespace liberty_code\validation\rule\test;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\rule\library\ConstRule;



class StringRuleTest extends DefaultRule
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
		// Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Init var
        $boolNotEmptyRequire = (
            isset($tabConfig['not_empty_require']) &&
            (intval($tabConfig['not_empty_require']) != 0)
        );
        $result = (
            is_string($value) &&
            (
                (!$boolNotEmptyRequire) ||
                (trim($value) != '')
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => 'string',
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => '%1$s must be a string'
        );
    }



}