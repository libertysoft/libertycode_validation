<?php
/**
 * Description :
 * This class allows to define validator rule class.
 * Validator rule uses validator object,
 * to validate data.
 * Can be consider is base of all rule type, using validator.
 *
 * Validator rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Validator rule uses the following specified validation configuration:
 * [
 *     Default rule validation configuration
 * ]
 *
 * Validator rule uses the following specified error configuration:
 * [
 *     Default rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\validation\validator\rule\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\rule\library\ConstValidatorRule;
use liberty_code\validation\validator\rule\exception\ValidatorInvalidFormatException;



/**
 * @method ValidatorInterface getObjValidator() Get validator object.
 * @method void setObjValidator(ValidatorInterface $objValidator) Set validator object.
 */
abstract class ValidatorRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ValidatorInterface $objValidator
     */
    public function __construct(
        ValidatorInterface $objValidator,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig
        );

        // Init validator
        $this->setObjValidator($objValidator);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstValidatorRule::DATA_KEY_DEFAULT_VALIDATOR))
        {
            $this->__beanTabData[ConstValidatorRule::DATA_KEY_DEFAULT_VALIDATOR] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstValidatorRule::DATA_KEY_DEFAULT_VALIDATOR
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstValidatorRule::DATA_KEY_DEFAULT_VALIDATOR:
                    ValidatorInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



}